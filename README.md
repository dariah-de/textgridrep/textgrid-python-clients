<!--
SPDX-FileCopyrightText: 2022 Georg-August-Universität Göttingen

SPDX-License-Identifier: CC0-1.0
-->

# TextGrid Python clients

The TextGrid Python clients provide access to the [TextGrid Repository](https://textgridrep.org/)
services [API](http://textgridlab.org/doc/services/index.html).


## Installation and Usage

```sh
pip install tgclients
```

Check the [usage docs](https://dariah-de.pages.gwdg.de/textgridrep/textgrid-python-clients/docs/getting-started.html) and the
Jupyter notebook examples to get started.

## Development

Look into the [development docs](https://dariah-de.pages.gwdg.de/textgridrep/textgrid-python-clients/docs/development.html).

## License

This project aims to be [REUSE compliant](https://api.reuse.software/info/gitlab.gwdg.de/dariah-de/textgridrep/textgrid-python-clients).
Original parts are licensed under AGPL-3.0-or-later.
Derivative code is licensed under the respective license of the original.
Documentation, configuration and generated code files are licensed under CC0-1.0.

## Badges

[![REUSE status](https://api.reuse.software/badge/gitlab.gwdg.de/dariah-de/textgridrep/textgrid-python-clients)](https://api.reuse.software/info/gitlab.gwdg.de/dariah-de/textgridrep/textgrid-python-clients)
[![PyPI](https://img.shields.io/pypi/v/tgclients)](https://pypi.org/project/tgclients/)
[![Coverage](https://gitlab.gwdg.de/dariah-de/textgridrep/textgrid-python-clients/badges/main/coverage.svg)](https://dariah-de.pages.gwdg.de/textgridrep/textgrid-python-clients/htmlcov/index.html)
