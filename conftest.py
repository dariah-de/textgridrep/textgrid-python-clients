# SPDX-FileCopyrightText: 2022 Georg-August-Universität Göttingen
#
# SPDX-License-Identifier: CC0-1.0


# pytest finds added options only in root conftest.py if not configured otherwise
def pytest_addoption(parser):
    parser.addoption(
        '--integration', action='store_true', default=False, help='run integration tests'
    )


# list marker if requested with 'pytest --markers'
def pytest_configure(config):
    config.addinivalue_line('markers', 'integration: mark a test as integration test')
