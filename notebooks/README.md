<!--
SPDX-FileCopyrightText: 2022 Georg-August-Universität Göttingen

SPDX-License-Identifier: CC0-1.0
-->

# Example Notebooks

The tgclients library project provides Jupyter Notebooks that document its usage.
Some are also part of the [documentation](https://dariah-de.pages.gwdg.de/textgridrep/textgrid-python-clients/docs/getting-started.html#example-jupyter-notebooks).

You can just copy them into any Jupyter notebook, but you may need to install tgclients there:

```
% pip install tgclients
```

## Run them locally

You can run a Jupyter lab with these notebooks available directly from this directory.
If you have already a [development venv](../docs/development.md) you may
activate the venv, enter this directory and run:

```bash
pip install jupyterlab tqdm ipywidgets
jupyter lab
```

Afterwards a web browser with jupyterlab should open, which offers this notebooks for opening.

If you did not initialize a development venv before you may also inititialize a venv and run
from this directory:

```bash
python -m venv venv
. venv/bin/activate
pip install jupyterlab tgclients
jupyter lab
```

## Unit Tests

Every notebook added here should get an entry in the
[notebooks integration test](../tests/integration/test_notebooks.py)
so we can track breakage on API changes.
