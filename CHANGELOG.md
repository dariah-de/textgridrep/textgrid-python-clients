# CHANGELOG


## v0.22.0 (2025-02-20)

### Features

- **TextgridAuth**: Add methods to search for users and list/add/remove roles from project to API
  ([`f771638`](https://gitlab.gwdg.de/dariah-de/textgridrep/textgrid-python-clients/-/commit/f771638c28d8e1b39537381b35d7473d6251c325))

- **TextgridPublish**: Add method for publish worldreadble to API
  ([`f5751e3`](https://gitlab.gwdg.de/dariah-de/textgridrep/textgrid-python-clients/-/commit/f5751e305aff6844c8832a1a53d3e08cb353fda2))


## v0.21.0 (2025-02-20)

### Features

- Add py.typed to package let mypy know it is providing type hints
  ([`2687d91`](https://gitlab.gwdg.de/dariah-de/textgridrep/textgrid-python-clients/-/commit/2687d91f01427c4d3a4ad13cec8ea9525bdce58c))


## v0.20.0 (2024-11-22)

### Bug Fixes

- **config**: Rename serialisation helper functions; add docstring to __str__()
  ([`6690e6e`](https://gitlab.gwdg.de/dariah-de/textgridrep/textgrid-python-clients/-/commit/6690e6e1ebd44909da27562e4ff1af1f61a2a8e0))

### Features

- Provide serialization for TextgridConfig
  ([`40a5214`](https://gitlab.gwdg.de/dariah-de/textgridrep/textgrid-python-clients/-/commit/40a521403f36737b54d4440304016ac65b6b38f3))


## v0.19.1 (2024-11-14)

### Bug Fixes

- **Jupyter Notebooks**: Also create new revisions of data not referenced in nonpublic index. add
  referenced works to uri list.
  ([`ea2e959`](https://gitlab.gwdg.de/dariah-de/textgridrep/textgrid-python-clients/-/commit/ea2e9594abd6a60f52cce33f12029e836b10a079))


## v0.19.0 (2024-08-01)

### Features

- Add uri param to tgcrud create api method
  ([`4ffeda2`](https://gitlab.gwdg.de/dariah-de/textgridrep/textgrid-python-clients/-/commit/4ffeda236fd42c2bdb47aa43086842d0be4ca997))


## v0.18.0 (2024-07-31)

### Features

- Add get-uri API call to crud client
  ([`d6f55ff`](https://gitlab.gwdg.de/dariah-de/textgridrep/textgrid-python-clients/-/commit/d6f55ffb1e7572fd2bf61cb757cb831463463a72))


## v0.17.0 (2024-06-20)


## v0.16.0 (2024-04-02)

### Features

- **TextgridCrudRequest**: Parse crud error message from meta html tag
  ([`0a0d32b`](https://gitlab.gwdg.de/dariah-de/textgridrep/textgrid-python-clients/-/commit/0a0d32bac53ba2dd2ba360a54fcdc35b220a0dff))


## v0.15.1 (2024-02-22)

### Bug Fixes

- **release**: Pypi api token was lost in gitlab-ci, cleanup and retry release
  ([`86ff0da`](https://gitlab.gwdg.de/dariah-de/textgridrep/textgrid-python-clients/-/commit/86ff0da8d903d6e10f53c482a0b040727ce68337))

### Features

- **Aggregator**: Api for (html) rendering
  ([`c85b0c2`](https://gitlab.gwdg.de/dariah-de/textgridrep/textgrid-python-clients/-/commit/c85b0c2ed9de76651740e9640262a03bdfc10bc5))

- Add jupyter notebook example to count all words in published text/xml
  ([`d7c7a2a`](https://gitlab.gwdg.de/dariah-de/textgridrep/textgrid-python-clients/-/commit/d7c7a2afc81de4f1893c66def58aa5ec5ea3a637))


## v0.14.0 (2024-01-08)


## v0.13.4 (2023-12-01)

### Performance Improvements

- Reuse tcp connections for crud, search and aggregator clients
  ([`c560c6e`](https://gitlab.gwdg.de/dariah-de/textgridrep/textgrid-python-clients/-/commit/c560c6edb54e3f4eb538943f940934e7bec16737))


## v0.13.3 (2023-11-27)

### Bug Fixes

- **TextgridMetadata::filename_from_metadata**: Navigation results with auth denied have no title
  and no format
  ([`145e899`](https://gitlab.gwdg.de/dariah-de/textgridrep/textgrid-python-clients/-/commit/145e8992e6b5e0f09eb7272456dbbee30749046d))


## v0.13.2 (2023-11-22)

### Bug Fixes

- Typo in __init__.py export
  ([`7e832fa`](https://gitlab.gwdg.de/dariah-de/textgridrep/textgrid-python-clients/-/commit/7e832fa3fafca543aeffd80ed59c3be91aeedfb1))

### Features

- Jupyter notebook for creating new revisions of published objects
  ([`4f5220b`](https://gitlab.gwdg.de/dariah-de/textgridrep/textgrid-python-clients/-/commit/4f5220b4fe2a77565060f8536abbd11add29745f))


## v0.13.1 (2023-07-18)

### Bug Fixes

- Typos in jupyter notebook
  ([`69c5c14`](https://gitlab.gwdg.de/dariah-de/textgridrep/textgrid-python-clients/-/commit/69c5c146114bd9a43c49d6e37bf789681b677a84))


## v0.13.0 (2023-07-18)

### Features

- Add notebook explaining auto completion with jupyter to docs
  ([`aaa78c6`](https://gitlab.gwdg.de/dariah-de/textgridrep/textgrid-python-clients/-/commit/aaa78c6a8a08e1d01d78be0e05a3da60eea89244))

- **TextgridCrudRequest**: Parse crud error message from meta html tag
  ([`acdf962`](https://gitlab.gwdg.de/dariah-de/textgridrep/textgrid-python-clients/-/commit/acdf962f1ed207eff9422ba883fb99cc2d271f69))

- Tgpublish copy has destUri now, add to tgclients databinding
  ([`c417804`](https://gitlab.gwdg.de/dariah-de/textgridrep/textgrid-python-clients/-/commit/c417804b4e279ed7f11c1a859b478288988ab753))

- Add publish method to publish client
  ([`f7e56c8`](https://gitlab.gwdg.de/dariah-de/textgridrep/textgrid-python-clients/-/commit/f7e56c8232d992ad03918d7cd0ddc21f3f300d6b))

- **TextgridPublish**: Add method for publish operation
  ([`f43ab4b`](https://gitlab.gwdg.de/dariah-de/textgridrep/textgrid-python-clients/-/commit/f43ab4bc432bcbb16f86c5167b845ea329884d87))

- Add tgpublish client for copy operation
  ([`7ce20ab`](https://gitlab.gwdg.de/dariah-de/textgridrep/textgrid-python-clients/-/commit/7ce20ab96c31e122eb28f5b0b95e30ef8770b9d6))
