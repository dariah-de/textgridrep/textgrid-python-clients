# SPDX-FileCopyrightText: 2022 Georg-August-Universität Göttingen
#
# SPDX-License-Identifier: CC0-1.0
import os

import pytest
from tgclients.aggregator import Aggregator
from tgclients.auth import TextgridAuth
from tgclients.config import TextgridConfig
from tgclients.crud import TextgridCrud, TextgridCrudRequest
from tgclients.publish import TextgridPublish, TextgridPublishRequest
from tgclients.search import TextgridSearch, TextgridSearchRequest


def pytest_collection_modifyitems(config, items):
    # Only run tests marked with 'integration' when --integration is passed
    # this option is added with pytest_addoption in root conftest.py
    if config.getoption('--integration'):
        # --integration given in cli: do not skip integration tests
        return
    skip_integration = pytest.mark.skip(reason='need --integration option to run')
    for item in items:
        if 'integration' in item.keywords:
            item.add_marker(skip_integration)


if os.getenv('TEXTGRID_HOST') is not None:
    _tgconfig = TextgridConfig(os.getenv('TEXTGRID_HOST'))
else:
    _tgconfig = TextgridConfig()


@pytest.fixture
def tgconfig() -> TextgridConfig:
    return _tgconfig


@pytest.fixture
def search_public() -> TextgridSearch:
    return TextgridSearch(_tgconfig)


@pytest.fixture
def searchrequest_public() -> TextgridSearchRequest:
    return TextgridSearchRequest(_tgconfig)


@pytest.fixture
def crud_public():
    return TextgridCrud(_tgconfig, for_publication=True)


@pytest.fixture
def crud():
    return TextgridCrud(_tgconfig)


@pytest.fixture
def crud_request_public():
    return TextgridCrudRequest(_tgconfig, for_publication=True)


@pytest.fixture
def crud_request():
    return TextgridCrudRequest(_tgconfig)


@pytest.fixture
def auth():
    return TextgridAuth(_tgconfig)


@pytest.fixture
def aggregator():
    return Aggregator(_tgconfig)


@pytest.fixture
def publish() -> TextgridPublish:
    return TextgridPublish(_tgconfig)


@pytest.fixture
def publish_request() -> TextgridPublishRequest:
    return TextgridPublishRequest(_tgconfig)
