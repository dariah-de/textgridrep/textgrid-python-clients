# SPDX-FileCopyrightText: 2022 Georg-August-Universität Göttingen
#
# SPDX-License-Identifier: LGPL-3.0-or-later


def test_public_aggregation_to_list(search_public):
    uri = 'textgrid:kv2p.0'
    res = search_public.list_aggregation(uri)
    # assert res.status_code == 200
    urilist = [res.textgrid_uri for res in res.result]
    print(urilist)
    assert urilist == ['textgrid:kv2x.0', 'textgrid:kv2t.0']
