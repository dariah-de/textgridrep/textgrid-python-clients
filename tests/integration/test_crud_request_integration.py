# SPDX-FileCopyrightText: 2022 Georg-August-Universität Göttingen
#
# SPDX-License-Identifier: LGPL-3.0-or-later

import os
from datetime import datetime

import pytest
from tgclients.metadata import TextgridMetadata

FIXTURE_PATH = './tests/fixtures/'


@pytest.mark.integration
class TestTextgridCrudIntegration:
    @staticmethod
    def test_read_public_data(crud_request):
        uri = 'textgrid:kv2q.0'
        res = crud_request.read_data(uri)
        assert 'Alice fing an sich zu langweilen;' in str(res.content, 'utf-8')

    @staticmethod
    def test_read_public_metadata(crud_request):
        uri = 'textgrid:kv2q.0'
        res = crud_request.read_metadata(uri)
        assert res.status_code == 200

    @staticmethod
    def test_read_public_metadata_umlaut(crud_request):
        uri = 'textgrid:s60f.0'
        res = crud_request.read_metadata(uri)
        assert res.status_code == 200

    @staticmethod
    def test_create_and_delete(crud_request):
        sid = os.getenv('SESSION_ID')
        project_id = os.getenv('PROJECT_ID')
        now = datetime.now().strftime('%Y-%m-%d-%H:%M:%S')
        metadata = TextgridMetadata.create(title='test ' + now, mimetype='text/xml')
        data = '<content>here</content>'

        res = crud_request.create_resource(sid, project_id, data, metadata)
        assert res.status_code == 200
        assert res.headers['Location'].startswith('textgrid:')
        textgrid_uri = res.headers['Location']
        res = crud_request.delete_resource(sid, textgrid_uri)
        assert res.status_code == 204

    @staticmethod
    def test_create_revision_and_delete(crud_request):
        sid = os.getenv('SESSION_ID')
        project_id = os.getenv('PROJECT_ID')
        now = datetime.now().strftime('%Y-%m-%d-%H:%M:%S')
        metadata = TextgridMetadata().create(title='test ' + now, mimetype='text/xml')
        data = '<content>here</content>'

        res_rev0 = crud_request.create_resource(sid, project_id, data, metadata)
        assert res_rev0.status_code == 200
        textgrid_uri = res_rev0.headers['Location']
        md_rev0 = res_rev0.text
        assert textgrid_uri.startswith('textgrid:')
        assert textgrid_uri.endswith('.0')

        res_rev1 = crud_request.create_revision(sid, project_id, textgrid_uri, data, md_rev0)
        assert res_rev1.status_code == 200
        textgrid_rev1_uri = res_rev1.headers['Location']
        md_rev1 = res_rev1.text
        assert textgrid_rev1_uri.startswith('textgrid:')
        assert textgrid_rev1_uri.endswith('.1')

        res_rev2 = crud_request.update_resource(
            sid, textgrid_rev1_uri, data, md_rev1, create_revision=True
        )
        assert res_rev2.status_code == 200
        textgrid_rev2_uri = res_rev2.headers['Location']
        assert textgrid_rev2_uri.startswith('textgrid:')
        assert textgrid_rev2_uri.endswith('.2')

        res = crud_request.delete_resource(sid, textgrid_rev2_uri)
        assert res.status_code == 204

        res = crud_request.delete_resource(sid, textgrid_rev1_uri)
        assert res.status_code == 204

        res = crud_request.delete_resource(sid, textgrid_uri)
        assert res.status_code == 204

    @staticmethod
    def test_create_update_read_and_delete(crud_request):
        sid = os.getenv('SESSION_ID')
        project_id = os.getenv('PROJECT_ID')
        now = datetime.now().strftime('%Y-%m-%d-%H:%M:%S')
        metadata = TextgridMetadata.create(title='test ' + now, mimetype='text/plain')
        # the content on creation
        cdata = 'created'

        res = crud_request.create_resource(sid, project_id, cdata, metadata)
        assert res.status_code == 200
        assert res.headers['Location'].startswith('textgrid:')
        textgrid_uri = res.headers['Location']

        res = crud_request.read_data(textgrid_uri, sid)
        # content matches?
        assert cdata == str(res.content, 'utf-8')

        umetadata = str(crud_request.read_metadata(textgrid_uri, sid).content, 'utf-8')
        # updated content
        udata = 'updated'
        res = crud_request.update_resource(sid, textgrid_uri, udata, umetadata)
        assert res.status_code == 200
        assert res.headers['Location'].startswith('textgrid:')

        res = crud_request.read_data(textgrid_uri, sid)
        # updated content matches?
        assert udata == str(res.content, 'utf-8')

        res = crud_request.delete_resource(sid, textgrid_uri)
        assert res.status_code == 204

    @staticmethod
    def test_streaming_create_update_read_and_delete(crud_request):
        sid = os.getenv('SESSION_ID')
        project_id = os.getenv('PROJECT_ID')
        now = datetime.now().strftime('%Y-%m-%d-%H_M_%S')
        metadata = TextgridMetadata.create(title='stream-test ' + now, mimetype='image/png')

        ###
        # the content on creation
        ###
        cfile_loc = FIXTURE_PATH + 'eulen150dpi.png'
        csize = os.path.getsize(cfile_loc)
        with open(cfile_loc, 'rb') as cdata:
            res = crud_request.create_resource(sid, project_id, cdata, metadata)

        assert res.status_code == 200
        assert res.headers['Location'].startswith('textgrid:')
        textgrid_uri = res.headers['Location']

        dl1_png_loc = '/tmp/unit-' + now + '-1.png'
        res = crud_request.read_data(textgrid_uri, sid)
        assert res.status_code == 200

        with open(dl1_png_loc, 'wb') as out_file:
            for chunk in res.iter_content(chunk_size=1024):
                out_file.write(chunk)

        assert os.path.getsize(dl1_png_loc) == csize

        ###
        # update content
        ###
        # metadata for update needs to have correct last-modified date
        umetadata = str(crud_request.read_metadata(textgrid_uri, sid).content, 'utf-8')
        ufile_loc = FIXTURE_PATH + 'eulen300dpi.png'
        usize = os.path.getsize(ufile_loc)
        with open(ufile_loc, 'rb') as udata:
            res = crud_request.update_resource(sid, textgrid_uri, udata, umetadata)

        assert res.status_code == 200
        assert res.headers['Location'].startswith('textgrid:')

        dl2_png_loc = '/tmp/unit-' + now + '-2.png'
        res = crud_request.read_data(textgrid_uri, sid)
        assert res.status_code == 200

        with open(dl2_png_loc, 'wb') as out_file:
            for chunk in res.iter_content(chunk_size=1024):
                out_file.write(chunk)

        assert os.path.getsize(dl2_png_loc) == usize

        ###
        # cleanup
        ###
        os.remove(dl1_png_loc)
        os.remove(dl2_png_loc)
        res = crud_request.delete_resource(sid, textgrid_uri)
        assert res.status_code == 204

    @staticmethod
    def test_get_uri(crud_request):
        sid = os.getenv('SESSION_ID')
        res = crud_request.get_uri(sid, 2)
        assert res.status_code == 200
        assert len(res.text.splitlines()) == 2
