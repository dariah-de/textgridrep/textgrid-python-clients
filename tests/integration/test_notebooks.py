# SPDX-FileCopyrightText: 2022 Georg-August-Universität Göttingen
#
# SPDX-License-Identifier: LGPL-3.0-or-later
import os

import pytest
from testbook import testbook


@pytest.mark.integration
class TestNotebooks:
    @staticmethod
    @testbook('notebooks/Kallimachos Import.ipynb')
    def test_notebook(tb, crud, auth, tgconfig):
        tb.execute_cell('download')
        tb.execute_cell('extract')
        assert len(tb.ref('ids')) > 1

        sid = os.getenv('SESSION_ID')
        host = tgconfig.host
        tb.inject(f"""
            SID = "{sid}"
            TG_HOST = "{host}"

            from tgclients.config import TextgridConfig
            config = TextgridConfig(TG_HOST)
        """)
        tb.execute_cell('create-project')
        project_id = tb.ref('project_id')
        assert project_id.startswith('TGPR-')

        tb.execute_cell('create-collection')
        collection_uri = tb.ref('collection_uri')
        assert collection_uri.startswith('textgrid:')
        assert collection_uri.endswith('.0')

        tb.execute_cell('list-collection')
        # this test should work again when rdf4j federated repo is fixed
        # res = tb.execute_cell('list-collection')
        # assert 'Die Rumplhanni' in res['outputs'][0]['text']

        # cleanup
        crud.delete_resource(sid, collection_uri)
        auth.delete_project(sid, project_id)

    @staticmethod
    @testbook('notebooks/Read and search public data.ipynb', execute=True)
    def test_read_search_public(tb):
        assert tb.ref('title') == 'Alice im Wunderland'

    @staticmethod
    @testbook('notebooks/Retrieve text and metadata from editions.ipynb', execute=True)
    def test_retrieve_text_and_metadata_from_editions(tb):
        """check if notebook executes # noqa: DAR101"""
        # its OK to just check the notebook executes, and API is not broken

    @staticmethod
    @testbook('notebooks/List and read DigiBib.ipynb', execute=True)
    def test_list_and_read_digibib(tb):
        """check if notebook executes # noqa: DAR101"""
        # its OK to just check the notebook executes, and API is not broken # noqa: DAR101

    @staticmethod
    @testbook('notebooks/Count_all_Words.ipynb', execute=True)
    def test_count_all_words(tb):
        """check if notebook executes # noqa: DAR101"""
        # its OK to just check the notebook executes, and API is not broken
