# SPDX-FileCopyrightText: 2022 Georg-August-Universität Göttingen
#
# SPDX-License-Identifier: LGPL-3.0-or-later

import os
from datetime import datetime

import pytest
from tgclients import TextgridAuthException
from tgclients.config import TEST_SERVER
from zeep.exceptions import TransportError


@pytest.mark.integration
class TestTextgridAuthIntegration:
    @staticmethod
    def test_list_assigned_projects(auth):
        sid = os.getenv('SESSION_ID')
        projects = auth.list_assigned_projects(sid)
        assert isinstance(projects, list)

    @staticmethod
    def test_list_assigned_projects_sidless(auth):
        projects = auth.list_assigned_projects('')
        assert isinstance(projects, list)
        assert len(projects) == 0

    @staticmethod
    def test_list_assigned_projects_wrong_sid(auth):
        with pytest.raises(TextgridAuthException):
            auth.list_assigned_projects('nosid')

    @staticmethod
    def test_list_all_projects(auth):
        projects = auth.list_all_projects()
        assert isinstance(projects, list)
        assert len(projects) > 35

    @staticmethod
    def test_get_project_description(auth, tgconfig):
        if tgconfig.host == TEST_SERVER:
            pytest.skip('test server does not have digibib, skipping test')
        project_info = auth.get_project_description('TGPR-372fe6dc-57f2-6cd4-01b5-2c4bbefcfd3c')
        assert project_info.name == 'Digitale Bibliothek'

    @staticmethod
    def test_create_and_delete(auth):
        sid = os.getenv('SESSION_ID')
        now = datetime.now().strftime('%Y-%m-%d-%H:%M:%S')
        project_id = auth.create_project(
            sid,
            'Integration test project from tgclients ' + now,
            'Integration test project from tgclients',
        )
        assert len(project_id) > 1
        res = auth.delete_project(sid, project_id)
        assert res is True

    @staticmethod
    def test_delete_non_existing_project(auth):
        sid = os.getenv('SESSION_ID')
        project_id = 'TGPR-NOT-EXISTING'
        res = auth.delete_project(sid, project_id)
        assert res is False

    @staticmethod
    def test_create_without_valid_sid(auth):
        sid = 'no-valid-sid'
        with pytest.raises(TextgridAuthException):
            auth.create_project(
                sid,
                'Integration test project from tgclients',
                'Integration test project from tgclients',
                False,
            )

    @staticmethod
    def test_delete_without_valid_sid(auth):
        sid = 'no-valid-sid'
        # we should have no right to delete this project, especially without sid
        # but in reality project emptiness is evaluated before sid (see capture in test_auth.py)
        project_id = os.getenv('PROJECT_ID')
        with pytest.raises(TextgridAuthException):
            auth.delete_project(sid, project_id)

    @staticmethod
    def test_add_role_without_valid_sid(auth):
        sid = 'no-valid-sid'
        # we should have no right change project, especially without sid
        project_id = os.getenv('PROJECT_ID')
        eppn = 'pytest@dariah.eu'
        with pytest.raises(TextgridAuthException):
            auth.add_observer_to_project(sid, project_id, eppn)
        with pytest.raises(TextgridAuthException):
            auth.add_admin_to_project(sid, project_id, eppn)
        with pytest.raises(TextgridAuthException):
            auth.add_manager_to_project(sid, project_id, eppn)
        with pytest.raises(TextgridAuthException):
            auth.add_editor_to_project(sid, project_id, eppn)

    @staticmethod
    def test_remove_roles_without_valid_sid(auth):
        sid = 'no-valid-sid'
        # we should have no right change project, especially without sid
        project_id = os.getenv('PROJECT_ID')
        eppn = 'pytest@dariah.eu'
        with pytest.raises(TextgridAuthException):
            auth.remove_observer_from_project(sid, project_id, eppn)
        with pytest.raises(TextgridAuthException):
            auth.remove_admin_from_project(sid, project_id, eppn)
        with pytest.raises(TextgridAuthException):
            auth.remove_manager_from_project(sid, project_id, eppn)
        with pytest.raises(TextgridAuthException):
            auth.remove_editor_from_project(sid, project_id, eppn)

    @staticmethod
    def test_add_remove_observer_role(auth):
        sid = os.getenv('SESSION_ID')
        project_id = os.getenv('PROJECT_ID')
        eppn = 'crud.junit1@textgrid.de'
        res = auth.add_observer_to_project(sid, project_id, eppn)
        assert res  # res shall be True
        res = auth.remove_observer_from_project(sid, project_id, eppn)
        assert res

    @staticmethod
    def test_add_remove_manager_role(auth):
        sid = os.getenv('SESSION_ID')
        project_id = os.getenv('PROJECT_ID')
        eppn = 'crud.junit1@textgrid.de'
        res = auth.add_manager_to_project(sid, project_id, eppn)
        assert res  # res shall be True
        res = auth.remove_manager_from_project(sid, project_id, eppn)
        assert res

    @staticmethod
    def test_add_remove_admin_role(auth):
        sid = os.getenv('SESSION_ID')
        project_id = os.getenv('PROJECT_ID')
        eppn = 'crud.junit1@textgrid.de'
        res = auth.add_admin_to_project(sid, project_id, eppn)
        assert res  # res shall be True
        res = auth.remove_admin_from_project(sid, project_id, eppn)
        assert res

    @staticmethod
    def test_add_remove_editor_role(auth):
        sid = os.getenv('SESSION_ID')
        project_id = os.getenv('PROJECT_ID')
        eppn = 'crud.junit1@textgrid.de'
        res = auth.add_admin_to_project(sid, project_id, eppn)
        assert res  # res shall be True
        res = auth.remove_admin_from_project(sid, project_id, eppn)
        assert res

    @staticmethod
    def test_eppn_for_sid_without_valid_sid(auth):
        sid = 'no-valid-sid'
        with pytest.raises(TextgridAuthException):
            auth.get_eppn_for_sid(sid)

    @staticmethod
    def test_get_ids_wrong_sid(auth):
        sid = 'no-valid-sid'
        with pytest.raises(TextgridAuthException):
            auth.get_ids(sid, 'test', '', '')
