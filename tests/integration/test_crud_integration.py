# SPDX-FileCopyrightText: 2022 Georg-August-Universität Göttingen
#
# SPDX-License-Identifier: LGPL-3.0-or-later

import os
from datetime import datetime

import pytest
from tgclients.crud import TextgridCrudException
from tgclients.metadata import TextgridMetadata

FIXTURE_PATH = './tests/fixtures/'


@pytest.mark.integration
class TestTextgridCrudIntegration:
    @staticmethod
    def test_read_public_data(crud):
        uri = 'textgrid:kv2q.0'
        res = crud.read_data(uri)
        assert 'Alice fing an sich zu langweilen;' in str(res.content, 'utf-8')

    @staticmethod
    def test_read_public_metadata(crud):
        uri = 'textgrid:kv2q.0'
        res = crud.read_metadata(uri)
        title = res.object_value.generic.provided.title[0]
        assert title == 'Alice im Wunderland'

    @staticmethod
    def test_read_public_metadata_umlaut(crud):
        uri = 'textgrid:s60f.0'
        res = crud.read_metadata(uri)
        title = res.object_value.generic.provided.title[0]
        assert title == 'Märchen'

    @staticmethod
    def test_read_wrong_uri(crud):
        uri = 'textgrid:notexistent'
        with pytest.raises(TextgridCrudException):
            crud.read_metadata(uri)

    @staticmethod
    def test_create_and_delete(crud):
        sid = os.getenv('SESSION_ID')
        project_id = os.getenv('PROJECT_ID')
        now = datetime.now().strftime('%Y-%m-%d-%H:%M:%S')
        metadata = TextgridMetadata().build(title='test ' + now, mimetype='text/xml')
        data = '<content>here</content>'

        res = crud.create_resource(sid, project_id, data, metadata)
        textgrid_uri = res.object_value.generic.generated.textgrid_uri.value
        assert textgrid_uri.startswith('textgrid:')
        res = crud.delete_resource(sid, textgrid_uri)
        assert res.status_code == 204

    @staticmethod
    def test_create_revision_and_delete(crud):
        sid = os.getenv('SESSION_ID')
        project_id = os.getenv('PROJECT_ID')
        now = datetime.now().strftime('%Y-%m-%d-%H:%M:%S')
        metadata = TextgridMetadata().build(title='test ' + now, mimetype='text/xml')
        data = '<content>here</content>'

        md_rev0 = crud.create_resource(sid, project_id, data, metadata)
        textgrid_uri = md_rev0.object_value.generic.generated.textgrid_uri.value
        assert textgrid_uri.startswith('textgrid:')
        assert textgrid_uri.endswith('.0')
        assert md_rev0.object_value.generic.generated.revision == 0

        md_rev1 = crud.create_revision(sid, project_id, textgrid_uri, data, md_rev0)
        textgrid_rev1_uri = md_rev1.object_value.generic.generated.textgrid_uri.value
        assert textgrid_rev1_uri.startswith('textgrid:')
        assert textgrid_rev1_uri.endswith('.1')
        assert md_rev1.object_value.generic.generated.revision == 1

        md_rev2 = crud.update_resource(sid, textgrid_rev1_uri, data, md_rev1, create_revision=True)
        textgrid_rev2_uri = md_rev2.object_value.generic.generated.textgrid_uri.value
        assert textgrid_rev2_uri.startswith('textgrid:')
        assert textgrid_rev2_uri.endswith('.2')
        assert md_rev2.object_value.generic.generated.revision == 2

        res = crud.delete_resource(sid, textgrid_rev2_uri)
        assert res.status_code == 204

        res = crud.delete_resource(sid, textgrid_rev1_uri)
        assert res.status_code == 204

        res = crud.delete_resource(sid, textgrid_uri)
        assert res.status_code == 204

    @staticmethod
    def test_create_update_metadata_and_delete(crud):
        sid = os.getenv('SESSION_ID')
        project_id = os.getenv('PROJECT_ID')
        now = datetime.now().strftime('%Y-%m-%d-%H:%M:%S')
        title = 'test ' + now
        metadata = TextgridMetadata().build(title=title, mimetype='text/xml')
        data = '<content>here</content>'

        cmeta = crud.create_resource(sid, project_id, data, metadata)
        textgrid_uri = cmeta.object_value.generic.generated.textgrid_uri.value
        assert textgrid_uri.startswith('textgrid:')
        assert cmeta.object_value.generic.provided.title[0] == title

        newtitle = 'new title after ' + now
        cmeta.object_value.generic.provided.title = newtitle

        cmeta2 = crud.update_metadata(sid, textgrid_uri, cmeta)
        assert cmeta2.object_value.generic.provided.title[0] == newtitle

        res = crud.delete_resource(sid, textgrid_uri)
        assert res.status_code == 204

    @staticmethod
    def test_create_update_read_and_delete(crud):
        sid = os.getenv('SESSION_ID')
        project_id = os.getenv('PROJECT_ID')
        now = datetime.now().strftime('%Y-%m-%d-%H:%M:%S')
        metadata = TextgridMetadata().build(title='test ' + now, mimetype='text/plain')
        # the content on creation
        cdata = 'created'

        res = crud.create_resource(sid, project_id, cdata, metadata)
        textgrid_uri = res.object_value.generic.generated.textgrid_uri.value
        assert textgrid_uri.startswith('textgrid:')

        res = crud.read_data(textgrid_uri, sid)
        # content matches?
        assert cdata == str(res.content, 'utf-8')

        umetadata = crud.read_metadata(textgrid_uri, sid)
        # updated content
        udata = 'updated'
        res = crud.update_resource(sid, textgrid_uri, udata, umetadata)
        textgrid_uri = res.object_value.generic.generated.textgrid_uri.value
        assert textgrid_uri.startswith('textgrid:')

        res = crud.read_data(textgrid_uri, sid)
        # updated content matches?
        assert udata == str(res.content, 'utf-8')

        res = crud.delete_resource(sid, textgrid_uri)
        assert res.status_code == 204

    @staticmethod
    def test_streaming_create_update_read_and_delete(crud):
        sid = os.getenv('SESSION_ID')
        project_id = os.getenv('PROJECT_ID')
        now = datetime.now().strftime('%Y-%m-%d-%H_M_%S')
        metadata = TextgridMetadata().build(title='stream-test ' + now, mimetype='image/png')

        ###
        # the content on creation
        ###
        cfile_loc = FIXTURE_PATH + 'eulen150dpi.png'
        csize = os.path.getsize(cfile_loc)
        with open(cfile_loc, 'rb') as cdata:
            res = crud.create_resource(sid, project_id, cdata, metadata)

        textgrid_uri = res.object_value.generic.generated.textgrid_uri.value
        assert textgrid_uri.startswith('textgrid:')

        dl1_png_loc = '/tmp/unit-' + now + '-1.png'
        res = crud.read_data(textgrid_uri, sid)
        assert res.status_code == 200

        with open(dl1_png_loc, 'wb') as out_file:
            for chunk in res.iter_content(chunk_size=1024):
                out_file.write(chunk)

        assert os.path.getsize(dl1_png_loc) == csize

        ###
        # update content
        ###
        # metadata for update needs to have correct last-modified date
        umetadata = crud.read_metadata(textgrid_uri, sid)
        ufile_loc = FIXTURE_PATH + 'eulen300dpi.png'
        usize = os.path.getsize(ufile_loc)
        with open(ufile_loc, 'rb') as udata:
            res = crud.update_resource(sid, textgrid_uri, udata, umetadata)

        textgrid_uri = res.object_value.generic.generated.textgrid_uri.value
        assert textgrid_uri.startswith('textgrid:')

        dl2_png_loc = '/tmp/unit-' + now + '-2.png'
        res = crud.read_data(textgrid_uri, sid)
        assert res.status_code == 200

        with open(dl2_png_loc, 'wb') as out_file:
            for chunk in res.iter_content(chunk_size=1024):
                out_file.write(chunk)

        assert os.path.getsize(dl2_png_loc) == usize

        ###
        # cleanup
        ###
        os.remove(dl1_png_loc)
        os.remove(dl2_png_loc)
        res = crud.delete_resource(sid, textgrid_uri)
        assert res.status_code == 204

    @staticmethod
    def test_get_uri(crud):
        sid = os.getenv('SESSION_ID')
        uris = crud.get_uri(sid, 2)
        assert len(uris) == 2
        for uri in uris:
            assert uri.startswith('textgrid:')

    @staticmethod
    def test_get_uri_and_create(crud):
        sid = os.getenv('SESSION_ID')
        project_id = os.getenv('PROJECT_ID')
        uris = crud.get_uri(sid, 1)
        assert len(uris) == 1
        assert uris[0].startswith('textgrid:')

        now = datetime.now().strftime('%Y-%m-%d-%H:%M:%S')
        metadata = TextgridMetadata().build(title='test ' + now, mimetype='text/xml')
        data = '<content>here</content>'

        res = crud.create_resource(sid, project_id, data, metadata, uri=uris[0])
        textgrid_uri = res.object_value.generic.generated.textgrid_uri.value
        assert textgrid_uri == f'{uris[0]}.0'
        res = crud.delete_resource(sid, textgrid_uri)
        assert res.status_code == 204
