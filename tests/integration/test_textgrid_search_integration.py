# SPDX-FileCopyrightText: 2022 Georg-August-Universität Göttingen
#
# SPDX-License-Identifier: LGPL-3.0-or-later

import pytest
from tgclients.databinding.tgsearch import Response as SearchResponse
from tgclients.databinding.tgsearch import TextgridUris


@pytest.mark.integration
class TestTextgridSearchIntegration:
    @staticmethod
    def test_public_info(search_public):
        uri = 'textgrid:kv2q.0'
        res: SearchResponse = search_public.info(uri)
        assert res.result[0].object_value.generic.provided.title[0] == 'Alice im Wunderland'
        assert res.result[0].object_value.generic.provided.format == 'text/xml'

    @staticmethod
    def test_public_list_project_root(search_public):
        # TODO: list a not to big public project root
        pass

    @staticmethod
    def test_public_aggregation_list(search_public):
        uri = 'textgrid:kv2p.0'
        res: SearchResponse = search_public.list_aggregation(uri)
        assert len(res.result) == 2

    @staticmethod
    def test_public_search_filter_format(search_public):
        filters = ['format:text/xml']
        res: SearchResponse = search_public.search(filters=filters)
        assert int(res.hits) > 100
        for r in res.result:
            assert r.object_value.generic.provided.format == 'text/xml'

    @staticmethod
    def test_public_search_filter_format_project(search_public):
        pid = 'TGPR-372fe6dc-57f2-6cd4-01b5-2c4bbefcfd3c'
        filters = ['format:text/xml', 'project.id:' + pid]
        res: SearchResponse = search_public.search(filters=filters)
        assert int(res.hits) > 100
        for r in res.result:
            assert r.object_value.generic.provided.format == 'text/xml'
            assert r.object_value.generic.generated.project.id == pid

    @staticmethod
    def test_edition_work_metadata_for(search_public):
        uri = 'textgrid:kv2q.0'
        res: SearchResponse = search_public.edition_work_metadata_for(uri)
        print(res)
        assert len(res.result) == 3
        formats = [hit.object_value.generic.provided.format for hit in res.result]
        assert 'text/xml' in formats
        assert 'text/tg.edition+tg.aggregation+xml' in formats
        assert 'text/tg.work+xml' in formats

    @staticmethod
    def test_children(search_public):
        uri = 'textgrid:kv2t.0'
        res: TextgridUris = search_public.children(uri)
        assert 'textgrid:kv2q.0' in res.textgrid_uri
        assert 'textgrid:kv2r.0' in res.textgrid_uri
