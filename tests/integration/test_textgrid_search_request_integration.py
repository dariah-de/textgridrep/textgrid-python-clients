# SPDX-FileCopyrightText: 2022 Georg-August-Universität Göttingen
#
# SPDX-License-Identifier: LGPL-3.0-or-later

import pytest


@pytest.mark.integration
class TestTextgridSearchRequestIntegration:
    @staticmethod
    def test_public_info(searchrequest_public):
        uri = 'textgrid:kv2q.0'

        res = searchrequest_public.info(uri)
        assert res.status_code == 200
        assert res.content

    @staticmethod
    def test_public_aggregation_list(searchrequest_public):
        uri = 'textgrid:kv2p.0'
        res = searchrequest_public.list_aggregation(uri)
        assert res.status_code == 200

    @staticmethod
    def test_public_search_filter(searchrequest_public):
        filters = ['format:text/xml']
        res = searchrequest_public.search(filters=filters)
        assert res.status_code == 200
