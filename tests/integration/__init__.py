# SPDX-FileCopyrightText: 2022 Georg-August-Universität Göttingen
#
# SPDX-License-Identifier: LGPL-3.0-or-later

import os
from datetime import datetime
from typing import List

from tgclients import TextgridCrud
from tgclients.databinding.textgrid_metadata_2010 import CollectionType, ItemType, PersonType
from tgclients.metadata import TextgridMetadata
from tgclients.utils import Utils


def create_test_resource(crud: TextgridCrud, name: str, set_rightsholder: bool = False) -> str:
    """Create a test resource in PROJECT_ID with SESSION_ID.
       An xml file with datetime in title

    Args:
        crud (TextgridCrud): instance of tgcrud
        name (str): name part of object
        set_rightsholder (bool): wether to set a rightsholder

    Returns:
        str: textgriduri of object created
    """
    sid = os.getenv('SESSION_ID')
    project_id = os.getenv('PROJECT_ID')
    now = datetime.now().strftime('%Y-%m-%d-%H:%M:%S')
    metadata = TextgridMetadata().build(title=name + '_testresource ' + now, mimetype='text/xml')

    if set_rightsholder:
        metadata.object_value.item = ItemType(rights_holder='integration test agent')

    data = '<content>here</content>'

    res = crud.create_resource(sid, project_id, data, metadata)
    textgrid_uri = res.object_value.generic.generated.textgrid_uri.value
    assert textgrid_uri.startswith('textgrid:')
    return textgrid_uri


def create_test_readme(crud: TextgridCrud, set_rightsholder: bool = False) -> str:
    """Create a README.md in PROJECT_ID with SESSION_ID.

    Args:
        crud (TextgridCrud): instance of tgcrud
        set_rightsholder (bool): wether to set a rightsholder

    Returns:
        str: textgriduri of object created
    """
    sid = os.getenv('SESSION_ID')
    project_id = os.getenv('PROJECT_ID')
    metadata = TextgridMetadata().build(title='README.md', mimetype='text/markdown')

    if set_rightsholder:
        metadata.object_value.item = ItemType(rights_holder='integration test agent')

    data = '# Title\n## Subtitle\n\nContent.'
    res = crud.create_resource(sid, project_id, data, metadata)
    textgrid_uri = res.object_value.generic.generated.textgrid_uri.value
    assert textgrid_uri.startswith('textgrid:')
    return textgrid_uri


def create_test_collection(crud: TextgridCrud, name: str, set_rightsholder: bool = False) -> dict:
    """Create a test resource inside a test collection in PROJECT_ID with SESSION_ID.
       all with datetime in title

    Args:
        crud (TextgridCrud): instance of tgcrud
        name (str): name part of object
        set_rightsholder (bool): wether to set a rightsholder

    Returns:
        dict: collection_uri and child_uri
    """
    sid = os.getenv('SESSION_ID')
    project_id = os.getenv('PROJECT_ID')
    now = datetime.now().strftime('%Y-%m-%d-%H:%M:%S')

    xml_obj_uri = create_test_resource(crud, name + '_xmlobj', set_rightsholder)

    base_uri = xml_obj_uri.split('.')[0]
    collection_data = Utils.list_to_aggregation('', [base_uri])
    collection_meta = TextgridMetadata().build(
        title=name + '_testcollection ' + now, mimetype='text/tg.collection+tg.aggregation+xml'
    )

    if set_rightsholder:
        collection_meta.object_value.collection = CollectionType(
            collector=PersonType('test collector')
        )

    res = crud.create_resource(sid, project_id, collection_data, collection_meta)
    textgrid_uri = res.object_value.generic.generated.textgrid_uri.value
    assert textgrid_uri.startswith('textgrid:')

    return {'collection_uri': textgrid_uri, 'child_uri': xml_obj_uri}


def delete_test_resources(crud: TextgridCrud, uris: List[str]):
    """Delete list of resources

    Args:
        crud (TextgridCrud): instance of tgcrud
        uris (List[str]): uris
    """
    sid = os.getenv('SESSION_ID')
    for uri in uris:
        res = crud.delete_resource(sid, uri)
        assert res.status_code == 204
