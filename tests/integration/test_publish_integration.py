# SPDX-FileCopyrightText: 2022 Georg-August-Universität Göttingen
#
# SPDX-License-Identifier: LGPL-3.0-or-later

import os
import time

import pytest
from tgclients import TextgridCrudException
from tgclients.config import PROD_SERVER
from tgclients.databinding.tgpublish.tgpub import ErrorType, ProcessStatusType, StatusType

from . import (
    create_test_collection,
    create_test_readme,
    create_test_resource,
    delete_test_resources,
)


@pytest.mark.integration
class TestPublishIntegration:
    @staticmethod
    def test_copy_new_rev(publish, crud):
        sid = os.getenv('SESSION_ID')

        # create test object
        textgrid_uri = create_test_resource(crud, 'copy_inner_project_new_rev')
        # copy job
        jobid = publish.copy_to_new_revision(sid, textgrid_uri)

        # check status until 100% progress
        status = publish.get_status(jobid)
        while status.publish_status.progress < 100:
            time.sleep(0.2)
            status = publish.get_status(jobid)

        assert status.publish_status.progress == 100
        dest_uri = status.publish_object[0].dest_uri
        assert dest_uri.startswith('textgrid:')
        assert dest_uri.endswith('.1')

        # cleanup test object and copy
        delete_test_resources(crud, [textgrid_uri, dest_uri])

    @staticmethod
    def test_copy_recursive_new_rev(publish, crud):
        sid = os.getenv('SESSION_ID')

        # create test objects
        res = create_test_collection(crud, 'copy_inner_project_new_rev')
        textgrid_uri = res['collection_uri']
        child_uri = res['child_uri']

        jobid = publish.copy_to_new_revision(sid, textgrid_uri)

        status = publish.get_status(jobid)
        while status.publish_status.progress < 100:
            time.sleep(0.2)
            status = publish.get_status(jobid)

        assert status.publish_status.progress == 100
        dest_uri = status.publish_object[0].dest_uri
        child_dest_uri = status.publish_object[1].dest_uri
        assert dest_uri.startswith('textgrid:')
        assert dest_uri.endswith('.1')
        assert child_dest_uri.startswith('textgrid:')
        assert child_dest_uri.endswith('.1')

        # cleanup test object and copy
        delete_test_resources(crud, [textgrid_uri, child_uri, dest_uri, child_dest_uri])

    @staticmethod
    def test_copy_inner_project(publish, crud):
        sid = os.getenv('SESSION_ID')
        project_id = os.getenv('PROJECT_ID')

        textgrid_uri = create_test_resource(crud, 'copy_inner_project')

        jobid = publish.copy(sid, textgrid_uri, project_id)

        status = publish.get_status(jobid)
        while status.publish_status.progress < 100:
            time.sleep(0.2)
            status = publish.get_status(jobid)

        assert status.publish_status.progress == 100
        dest_uri = status.publish_object[0].dest_uri
        assert dest_uri.startswith('textgrid:')

        # cleanup test object and copy
        delete_test_resources(crud, [textgrid_uri, dest_uri])

    @staticmethod
    def test_copy(publish, crud):
        sid = os.getenv('SESSION_ID')
        project2_id = os.getenv('PROJECT2_ID')

        textgrid_uri = create_test_resource(crud, 'copy')

        jobid = publish.copy(sid, textgrid_uri, project2_id)

        status = publish.get_status(jobid)
        while status.publish_status.progress < 100:
            time.sleep(0.2)
            status = publish.get_status(jobid)

        assert status.publish_status.progress == 100
        dest_uri = status.publish_object[0].dest_uri
        assert dest_uri.startswith('textgrid:')

        # cleanup test object and copy
        delete_test_resources(crud, [textgrid_uri, dest_uri])

    @staticmethod
    def test_copy_recursive(publish, crud):
        sid = os.getenv('SESSION_ID')
        project2_id = os.getenv('PROJECT2_ID')

        # create test objects
        res = create_test_collection(crud, 'copy_recursive')
        textgrid_uri = res['collection_uri']
        child_uri = res['child_uri']

        jobid = publish.copy(sid, textgrid_uri, project2_id)

        status = publish.get_status(jobid)
        while status.publish_status.progress < 100:
            time.sleep(0.2)
            status = publish.get_status(jobid)

        assert publish.get_status(jobid).publish_status.progress == 100
        dest_uri = status.publish_object[0].dest_uri
        child_dest_uri = status.publish_object[1].dest_uri
        assert dest_uri.startswith('textgrid:')
        assert child_dest_uri.startswith('textgrid:')

        # cleanup test object and copy
        delete_test_resources(crud, [textgrid_uri, child_uri, dest_uri, child_dest_uri])

    @staticmethod
    def test_publish_dryrun_wrong_format(publish, crud):
        sid = os.getenv('SESSION_ID')

        # create resource
        textgrid_uri = create_test_resource(crud, 'publish_dryrun_wrong_format')
        # publish resource
        jobid = publish.publish(sid, textgrid_uri)

        status = publish.get_status(jobid)
        assert status.dry_run is True
        # ProcessStatusType moves from QUEUED to RUNNING to FAILED
        while status.publish_status.process_status != ProcessStatusType.FAILED:
            time.sleep(0.2)
            status = publish.get_status(jobid)

        assert status.publish_object[0].error[0].type_value == ErrorType.WRONG_CONTENT_TYPE

        # delete resource
        delete_test_resources(crud, [textgrid_uri])

    @staticmethod
    def test_publish_wrong_format(publish, crud, tgconfig):
        sid = os.getenv('SESSION_ID')

        if tgconfig.host == PROD_SERVER:
            pytest.skip('do not run publish tests on prod')

        textgrid_uri = create_test_resource(crud, 'publish_wrong_format')
        jobid = publish.publish(sid, textgrid_uri, dry_run=False)

        status = publish.get_status(jobid)
        assert status.dry_run is False
        # ProcessStatusType moves from QUEUED to RUNNING to FAILED
        while status.publish_status.process_status != ProcessStatusType.FAILED:
            time.sleep(0.2)
            status = publish.get_status(jobid)

        assert status.publish_object[0].error[0].type_value == ErrorType.WRONG_CONTENT_TYPE
        # delete resource
        delete_test_resources(crud, [textgrid_uri])

    @staticmethod
    def test_publish_dryrun_missing_rightsholder(publish, crud):
        sid = os.getenv('SESSION_ID')

        res = create_test_collection(crud, 'publish_dryrun_missing_rightsholder')
        textgrid_uri = res['collection_uri']
        child_uri = res['child_uri']
        # publish resource
        jobid = publish.publish(sid, textgrid_uri, dry_run=True)

        status = publish.get_status(jobid)
        assert status.dry_run is True
        # ProcessStatusType moves from QUEUED to RUNNING to FAILED
        while status.publish_status.process_status != ProcessStatusType.FAILED:
            time.sleep(0.2)
            status = publish.get_status(jobid)

        assert status.publish_object[0].error[0].type_value == ErrorType.MISSING_METADATA

        delete_test_resources(crud, [textgrid_uri, child_uri])

    @staticmethod
    def test_publish_dryrun(publish, crud):
        sid = os.getenv('SESSION_ID')

        res = create_test_collection(crud, 'publish_dryrun', set_rightsholder=True)
        textgrid_uri = res['collection_uri']
        child_uri = res['child_uri']
        # publish resource
        jobid = publish.publish(sid, textgrid_uri, dry_run=True)

        status = publish.get_status(jobid)
        assert status.dry_run is True
        # ProcessStatusType moves from QUEUED to RUNNING to FINISHED
        while status.publish_status.process_status != ProcessStatusType.FINISHED:
            time.sleep(0.2)
            status = publish.get_status(jobid)

        assert status.publish_object[0].status == StatusType.OK

        delete_test_resources(crud, [textgrid_uri, child_uri])

    @staticmethod
    def test_publish(publish, crud, tgconfig):
        sid = os.getenv('SESSION_ID')

        if tgconfig.host == PROD_SERVER:
            pytest.skip('do not run publish tests on prod')

        res = create_test_collection(crud, 'publish', set_rightsholder=True)
        textgrid_uri = res['collection_uri']
        # child_uri = res['child_uri']
        # publish resource
        jobid = publish.publish(sid, textgrid_uri, dry_run=False)

        status = publish.get_status(jobid)
        assert status.dry_run is False
        # ProcessStatusType moves from QUEUED to RUNNING to FINISHED
        while status.publish_status.process_status != ProcessStatusType.FINISHED:
            time.sleep(0.2)
            status = publish.get_status(jobid)

        assert status.publish_object[0].status == StatusType.OK

        # published objects are not deletable
        with pytest.raises(TextgridCrudException):
            res = crud.delete_resource(sid, textgrid_uri)
            assert res.status_code == 401

    @staticmethod
    def test_publish_worldreadable_dryrun_wrong_format(publish, crud):
        sid = os.getenv('SESSION_ID')

        textgrid_uri = create_test_resource(crud, 'no-readme', set_rightsholder=True)
        jobid = publish.publish_worldreadable(sid, textgrid_uri, dry_run=True)
        status = publish.get_status(jobid)
        assert status.dry_run is True
        # ProcessStatusType moves from QUEUED to RUNNING to FAILED
        while status.publish_status.process_status != ProcessStatusType.FAILED:
            time.sleep(0.2)
            status = publish.get_status(jobid)

        assert status.publish_object[0].error[0].type_value == ErrorType.WRONG_CONTENT_TYPE

        # delete resource
        delete_test_resources(crud, [textgrid_uri])

    @staticmethod
    def test_publish_worldreadable_wrong_format(publish, crud, tgconfig):
        sid = os.getenv('SESSION_ID')

        if tgconfig.host == PROD_SERVER:
            pytest.skip('do not run publish tests on prod')

        textgrid_uri = create_test_resource(crud, 'no-readme', set_rightsholder=True)
        jobid = publish.publish_worldreadable(sid, textgrid_uri, dry_run=False)
        status = publish.get_status(jobid)
        assert status.dry_run is False
        # ProcessStatusType moves from QUEUED to RUNNING to FAILED
        while status.publish_status.process_status != ProcessStatusType.FAILED:
            time.sleep(0.2)
            status = publish.get_status(jobid)

        assert status.publish_object[0].error[0].type_value == ErrorType.WRONG_CONTENT_TYPE
        # delete resource
        delete_test_resources(crud, [textgrid_uri])

    @staticmethod
    def test_publish_worldreadable_dryrun(publish, crud, tgconfig):
        sid = os.getenv('SESSION_ID')
        if tgconfig.host == PROD_SERVER:
            pytest.skip('do not run publish tests on prod')

        textgrid_uri = create_test_readme(crud, set_rightsholder=True)
        jobid = publish.publish_worldreadable(sid, textgrid_uri, dry_run=True)
        status = publish.get_status(jobid)
        assert status.dry_run is True
        # ProcessStatusType moves from QUEUED to RUNNING to FINISHED
        while status.publish_status.process_status != ProcessStatusType.FINISHED:
            time.sleep(0.2)
            status = publish.get_status(jobid)

        assert status.publish_object[0].status == StatusType.OK

        delete_test_resources(crud, [textgrid_uri])

    @staticmethod
    def test_publish_worldreadable(publish, crud, tgconfig):
        sid = os.getenv('SESSION_ID')
        if tgconfig.host == PROD_SERVER:
            pytest.skip('do not run publish tests on prod')

        textgrid_uri = create_test_readme(crud, set_rightsholder=True)
        jobid = publish.publish_worldreadable(sid, textgrid_uri, dry_run=False)
        status = publish.get_status(jobid)
        assert status.dry_run is False
        # ProcessStatusType moves from QUEUED to RUNNING to FINISHED
        while status.publish_status.process_status != ProcessStatusType.FINISHED:
            time.sleep(0.2)
            status = publish.get_status(jobid)

        assert status.publish_object[0].status == StatusType.OK

        with pytest.raises(TextgridCrudException):
            res = crud.delete_resource(sid, textgrid_uri)
            assert res.status_code == 401
