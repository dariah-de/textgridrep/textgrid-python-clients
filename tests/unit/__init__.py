# SPDX-FileCopyrightText: 2022 Georg-August-Universität Göttingen
#
# SPDX-License-Identifier: LGPL-3.0-or-later

FIXTURE_PATH = './tests/fixtures/'


def fileLoader(filename):
    def loader(req, context):
        with open(FIXTURE_PATH + filename, 'r') as f:
            return f.read()

    return loader


def binaryFileLoader(filename):
    def loader(req, context):
        with open(FIXTURE_PATH + filename, 'rb') as f:
            return f.read()

    return loader
