# SPDX-FileCopyrightText: 2022 Georg-August-Universität Göttingen
#
# SPDX-License-Identifier: LGPL-3.0-or-later

from tgclients.databinding.tgpublish.tgpub import ProcessStatusType, StatusType

from . import fileLoader


class TestPublish:
    @staticmethod
    def test_copy_inner_project_new_rev(requests_mock, publish, tgconfig):
        requests_mock.get(
            tgconfig.host
            + '/1.0/tgpublish/copy?sid=SESSION_ID&uri=textgrid:unit1.0&newRevision=true',
            status_code=204,
            text='tgcopy:unit1',
        )
        requests_mock.get(
            tgconfig.host + '/1.0/tgpublish/tgcopy:unit1/status',
            [
                {'text': fileLoader('publish/copy_new_rev_status1'), 'status_code': 200},
                {'text': fileLoader('publish/copy_new_rev_status2'), 'status_code': 200},
            ],
        )
        sid = 'SESSION_ID'  # does not matter for mocked requests
        textgrid_uri = 'textgrid:unit1.0'  # does not matter for mocked requests

        jobid = publish.copy_to_new_revision(sid, textgrid_uri)

        status = publish.get_status(jobid)
        while status.publish_status.progress < 100:
            status = publish.get_status(jobid)

        assert publish.get_status(jobid).publish_status.progress == 100

    @staticmethod
    def test_copy(requests_mock, publish, tgconfig):
        requests_mock.get(
            tgconfig.host
            + '/1.0/tgpublish/copy?sid=SESSION_ID&uri=textgrid:unit1.0&projectId=PROJECT_ID&newRevision=false',
            status_code=204,
            text='tgcopy:unit1',
        )
        requests_mock.get(
            tgconfig.host + '/1.0/tgpublish/tgcopy:unit1/status',
            [
                {'text': fileLoader('publish/copystatus1'), 'status_code': 200},
                {'text': fileLoader('publish/copystatus2'), 'status_code': 200},
            ],
        )
        sid = 'SESSION_ID'  # does not matter for mocked requests
        project_id = 'PROJECT_ID'  # does not matter for mocked requests
        textgrid_uri = 'textgrid:unit1.0'  # does not matter for mocked requests

        jobid = publish.copy(sid, textgrid_uri, project_id)

        status = publish.get_status(jobid)
        while status.publish_status.progress < 100:
            status = publish.get_status(jobid)

        assert publish.get_status(jobid).publish_status.progress == 100

    @staticmethod
    def test_publish(requests_mock, publish, tgconfig):
        requests_mock.get(
            tgconfig.host
            + '/1.0/tgpublish/textgrid:unit1.0/publish?sid=SESSION_ID&ignoreWarnings=False&dryRun=False',
            status_code=204,
            text='textgrid:unit1.0',
        )
        requests_mock.get(
            tgconfig.host + '/1.0/tgpublish/textgrid:unit1.0/status',
            [
                {'text': fileLoader('publish/publishstatus1'), 'status_code': 200},
                {'text': fileLoader('publish/publishstatus2'), 'status_code': 200},
            ],
        )
        sid = 'SESSION_ID'  # does not matter for mocked requests
        textgrid_uri = 'textgrid:unit1.0'  # does not matter for mocked requests

        # publish resource
        jobid = publish.publish(sid, textgrid_uri, dry_run=False)

        status = publish.get_status(jobid)
        assert status.dry_run is False
        # ProcessStatusType moves from QUEUED to RUNNING to FINISHED
        while status.publish_status.process_status != ProcessStatusType.FINISHED:
            status = publish.get_status(jobid)

        assert status.publish_object[0].status == StatusType.OK

    @staticmethod
    def test_publish_worldreadable(requests_mock, publish, tgconfig):
        requests_mock.get(
            tgconfig.host
            + '/1.0/tgpublish/textgrid:unit1.0/publishWorldreadable?sid=SESSION_ID&ignoreWarnings=False&dryRun=False',
            status_code=204,
            text='textgrid:unit1.0',
        )
        requests_mock.get(
            tgconfig.host + '/1.0/tgpublish/textgrid:unit1.0/status',
            [
                {'text': fileLoader('publish/publishstatus1'), 'status_code': 200},
                {'text': fileLoader('publish/publishstatus2'), 'status_code': 200},
            ],
        )
        sid = 'SESSION_ID'  # does not matter for mocked requests
        textgrid_uri = 'textgrid:unit1.0'  # does not matter for mocked requests

        # publish resource
        jobid = publish.publish_worldreadable(sid, textgrid_uri, dry_run=False)

        status = publish.get_status(jobid)
        assert status.dry_run is False
        # ProcessStatusType moves from QUEUED to RUNNING to FINISHED
        while status.publish_status.process_status != ProcessStatusType.FINISHED:
            status = publish.get_status(jobid)

        assert status.publish_object[0].status == StatusType.OK
