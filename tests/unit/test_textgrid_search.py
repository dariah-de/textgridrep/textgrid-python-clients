# SPDX-FileCopyrightText: 2022 Georg-August-Universität Göttingen
#
# SPDX-License-Identifier: LGPL-3.0-or-later

from tgclients.config import TextgridConfig
from tgclients.databinding.tgsearch import Response as SearchResponse
from tgclients.databinding.tgsearch import TextgridUris
from tgclients.search import TextgridSearch

from . import fileLoader


class TestTextgridSearch:
    @staticmethod
    def test_empty_constructor():
        """Not passing a config in constructor should provide a client for
        tgsearch public on production system"""
        tgs = TextgridSearch()
        assert tgs._url == TextgridConfig().search_public

    @staticmethod
    def test_public_info(requests_mock, search_public, tgconfig):
        requests_mock.get(
            tgconfig.host + '/1.0/tgsearch-public/info/textgrid:kv2q.0', text=fileLoader('kv2q.0')
        )

        uri = 'textgrid:kv2q.0'
        res: SearchResponse = search_public.info(uri)
        assert res.result[0].object_value.generic.provided.title[0] == 'Alice im Wunderland'
        assert res.result[0].object_value.generic.provided.format == 'text/xml'

    @staticmethod
    def test_public_list_project_root(requests_mock, search_public):
        # TODO: list a not to big public project root
        pass

    @staticmethod
    def test_public_aggregation_list(requests_mock, search_public, tgconfig):
        requests_mock.get(
            tgconfig.host + '/1.0/tgsearch-public/navigation/agg/textgrid:kv2p.0',
            text=fileLoader('agg_kv2p.0'),
        )
        uri = 'textgrid:kv2p.0'
        res: SearchResponse = search_public.list_aggregation(uri)
        assert len(res.result) == 2

    @staticmethod
    def test_public_search_filter_format(requests_mock, search_public, tgconfig):
        requests_mock.get(
            tgconfig.host + '/1.0/tgsearch-public/search?q=%2A&filter=format%3Atext%2Fxml',
            text=fileLoader('search/filter_format_xml.xml'),
        )
        filters = ['format:text/xml']
        res: SearchResponse = search_public.search(filters=filters)
        assert int(res.hits) > 100
        for r in res.result:
            assert r.object_value.generic.provided.format == 'text/xml'

    @staticmethod
    def test_public_search_filter_format_project(requests_mock, search_public, tgconfig):
        requests_mock.get(
            tgconfig.host
            + '/1.0/tgsearch-public/search?q=%2A&filter=format%3Atext%2Fxml&filter=project.id%3ATGPR-372fe6dc-57f2-6cd4-01b5-2c4bbefcfd3c',
            text=fileLoader('search/filter_format_project.xml'),
        )
        pid = 'TGPR-372fe6dc-57f2-6cd4-01b5-2c4bbefcfd3c'
        filters = ['format:text/xml', 'project.id:' + pid]
        res: SearchResponse = search_public.search(filters=filters)
        assert int(res.hits) > 100
        for r in res.result:
            assert r.object_value.generic.provided.format == 'text/xml'
            assert r.object_value.generic.generated.project.id == pid

    @staticmethod
    def test_edition_work_metadata_for(requests_mock, search_public, tgconfig):
        requests_mock.get(
            tgconfig.host + '/1.0/tgsearch-public/info/textgrid:kv2q.0/editionWorkMeta',
            text=fileLoader('search/kv2q.0.editionWorkMeta.xml'),
        )
        uri = 'textgrid:kv2q.0'
        res: SearchResponse = search_public.edition_work_metadata_for(uri)
        print(res)
        assert len(res.result) == 3
        formats = [hit.object_value.generic.provided.format for hit in res.result]
        assert 'text/xml' in formats
        assert 'text/tg.edition+tg.aggregation+xml' in formats
        assert 'text/tg.work+xml' in formats

    @staticmethod
    def test_children(requests_mock, search_public, tgconfig):
        requests_mock.get(
            tgconfig.host + '/1.0/tgsearch-public/info/textgrid:kv2t.0/children',
            text=fileLoader('search/kv2t.0.children.xml'),
        )
        uri = 'textgrid:kv2t.0'
        res: TextgridUris = search_public.children(uri)
        assert 'textgrid:kv2q.0' in res.textgrid_uri
        assert 'textgrid:kv2r.0' in res.textgrid_uri
