# SPDX-FileCopyrightText: 2022 Georg-August-Universität Göttingen
#
# SPDX-License-Identifier: LGPL-3.0-or-later


# to sniff soap bodys you may disable gzip with the following snipper in auth.py:
# from requests import Session
# from zeep.transports import Transport
#        session = Session()
#        session.headers.update({'Accept-Encoding' : 'br'})
#        transport = Transport(session=session)
#        client = Client(self._config.extra_crud_wsdl, transport=transport)

import pytest
from tgclients import TextgridAuthException
from tgclients.auth import TextgridAuth
from zeep.exceptions import TransportError

SOAP_RESPONSE_TRUE = {
    'status_code': 200,
    'text': """<?xml version="1.0" encoding="UTF-8"?>
    <SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns1="http://textgrid.info/namespaces/middleware/tgauth">
    <SOAP-ENV:Body>
        <ns1:booleanResponse>
            <result>true</result>
        </ns1:booleanResponse>
    </SOAP-ENV:Body>
    </SOAP-ENV:Envelope>""",
}


class TestTextgridAuth:
    @staticmethod
    def test_empty_constructor():
        """Not passing a config in constructor should provide a client for
        the prodction system textgridlab.org"""
        tga = TextgridAuth()
        assert tga._config.auth_address.startswith('https://textgridlab.org/1.0/')

    @staticmethod
    def test_list_assigned_projects(auth, tgconfig, requests_mock):
        soap_mock = """<?xml version="1.0" encoding="UTF-8"?>
            <SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns1="http://textgrid.info/namespaces/middleware/tgauth">
            <SOAP-ENV:Body>
                <ns1:rolesetResponse>
                    <role>TGPR-1d3e59ae-a298-c816-febe-d539236e9c9b</role>
                    <role>TGPR-51eddadd-2062-cbba-694d-ca98e1ee558d</role>
                    <role>TGPR-54ee8162-3bbe-9e45-bf5a-8cfca3605b84</role>
                </ns1:rolesetResponse>
            </SOAP-ENV:Body>
            </SOAP-ENV:Envelope>"""

        requests_mock.post(
            tgconfig.host + '/1.0/tgauth/tgextra.php',
            status_code=200,
            text=soap_mock,
        )
        sid = 'SESSION_ID'
        projects = auth.list_assigned_projects(sid)
        assert isinstance(projects, list)

    @staticmethod
    def test_list_assigned_projects_wrong_sid(auth, tgconfig, requests_mock):
        requests_mock.post(
            tgconfig.host + '/1.0/tgauth/tgextra.php',
            status_code=500,
        )
        with pytest.raises(TextgridAuthException):
            auth.list_assigned_projects('nosid')

    @staticmethod
    def test_list_all_projects(auth, tgconfig, requests_mock):
        soap_mock = """<?xml version="1.0" encoding="UTF-8"?>
            <SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns1="http://textgrid.info/namespaces/middleware/tgauth">
            <SOAP-ENV:Body>
                <ns1:getAllProjectsResponse>
                    <project>
                        <id>TGPR-003112a5-18be-6e3d-bd93-673f37cb05f3</id>
                        <description>fugiblugi</description>
                        <name>fugiblugi</name>
                    </project>
                    <project>
                        <id>TGPR-003112a5-18be-6e3d-bd93-673f37cb05f4</id>
                        <description>fugiblugi</description>
                        <name>fugiblugili</name>
                    </project>
                </ns1:getAllProjectsResponse>
            </SOAP-ENV:Body>
            </SOAP-ENV:Envelope>"""

        requests_mock.post(
            tgconfig.host + '/1.0/tgauth/tgextra.php',
            status_code=200,
            text=soap_mock,
        )
        projects = auth.list_all_projects()
        assert isinstance(projects, list)
        assert len(projects) > 1

    @staticmethod
    def test_get_project_description(auth, tgconfig, requests_mock):
        soap_mock = """<?xml version="1.0" encoding="UTF-8"?>
            <SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns1="http://textgrid.info/namespaces/middleware/tgauth">
            <SOAP-ENV:Body>
                <ns1:getProjectDescriptionResponse>
                    <project>
                        <id>TGPR-372fe6dc-57f2-6cd4-01b5-2c4bbefcfd3c</id>
                        <description>TEI-Daten der Digitalen Bibliothek</description>
                        <name>Digitale Bibliothek</name>
                    </project>
                </ns1:getProjectDescriptionResponse>
            </SOAP-ENV:Body>
            </SOAP-ENV:Envelope>"""
        requests_mock.post(
            tgconfig.host + '/1.0/tgauth/tgextra.php',
            status_code=200,
            text=soap_mock,
        )
        project_info = auth.get_project_description('TGPR-372fe6dc-57f2-6cd4-01b5-2c4bbefcfd3c')
        assert project_info.name == 'Digitale Bibliothek'

    @staticmethod
    def test_create_and_delete(auth, tgconfig, requests_mock):
        soap_mocks = [
            {
                'status_code': 200,
                'text': """<?xml version="1.0" encoding="UTF-8"?>
                <SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns1="http://textgrid.info/namespaces/middleware/tgauth">
                <SOAP-ENV:Body>
                    <ns1:createProjectResponse>
                        <projectId>TGPR-fae55e90-63ba-24a0-695c-67b60971a0ee</projectId>
                    </ns1:createProjectResponse>
                </SOAP-ENV:Body></SOAP-ENV:Envelope>""",
            },
            SOAP_RESPONSE_TRUE,
            SOAP_RESPONSE_TRUE,
            SOAP_RESPONSE_TRUE,
        ]
        requests_mock.post(
            tgconfig.host + '/1.0/tgauth/tgextra.php',
            soap_mocks,
        )
        requests_mock.post(
            tgconfig.host + '/1.0/tgauth/tgextra-crud.php',
            status_code=200,
            text="""<?xml version="1.0" encoding="UTF-8"?>
                <SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns1="http://textgrid.info/namespaces/middleware/tgauth-crud">
                <SOAP-ENV:Body>
                    <ns1:getEPPNResponse>
                        <eppn>testitest@textgrid.de</eppn>
                    </ns1:getEPPNResponse>
                </SOAP-ENV:Body>
                </SOAP-ENV:Envelope>""",
        )
        sid = 'SESSION_ID'
        project_id = auth.create_project(
            sid,
            'Integration test project from tgclients ',
            'Integration test project from tgclients',
        )
        assert len(project_id) > 1
        res = auth.delete_project(sid, project_id)
        assert res is True

    @staticmethod
    def test_create_without_valid_sid(auth, tgconfig, requests_mock):
        soap_mock = """<?xml version="1.0" encoding="UTF-8"?>
            <SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/">
            <SOAP-ENV:Body><SOAP-ENV:Fault>
                <faultcode>authenticationFault</faultcode>
                <faultstring>2</faultstring>
                <faultactor>TgExtra</faultactor>
                <detail>
                    <faultNo>2</faultNo>
                    <faultMessage>This function requires an authentication and authorization.</faultMessage>
                    <cause>You have to login to be able to create a new project.</cause>
                </detail>
                </SOAP-ENV:Fault>
            </SOAP-ENV:Body></SOAP-ENV:Envelope>"""

        requests_mock.post(
            tgconfig.host + '/1.0/tgauth/tgextra.php',
            status_code=500,
            text=soap_mock,
        )
        sid = 'no-valid-sid'
        with pytest.raises(TextgridAuthException):
            auth.create_project(
                sid,
                'Integration test project from tgclients',
                'Integration test project from tgclients',
                False,
            )

    @staticmethod
    def test_delete_without_valid_sid(auth, tgconfig, requests_mock):
        soap_mock = """<?xml version="1.0" encoding="UTF-8"?>
            <SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/">
            <SOAP-ENV:Body>
            <SOAP-ENV:Fault>
                <faultcode>notEmptyFault</faultcode>
                <faultstring>7</faultstring>
                <faultactor>TgExtra</faultactor>
                <detail>
                    <faultNo>7</faultNo>
                    <faultMessage>Project could not be deleted</faultMessage>
                    <cause>There are still 54 resources belonging to this project.</cause>
                </detail>
            </SOAP-ENV:Fault></SOAP-ENV:Body></SOAP-ENV:Envelope>"""  # interestingly resources are evaluated before sid
        requests_mock.post(
            tgconfig.host + '/1.0/tgauth/tgextra.php',
            status_code=500,
            text=soap_mock,
        )
        sid = 'no-valid-sid'
        # we should have no right to delete this project, especially without sid
        # using a non existing project does not raise the exception
        project_id = 'PROJECT_ID'
        with pytest.raises(TextgridAuthException):
            auth.delete_project(sid, project_id)

    @staticmethod
    def test_add_role_without_valid_sid(auth, tgconfig, requests_mock):
        soap_mock = """<?xml version="1.0" encoding="UTF-8"?>
            <SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns1="http://textgrid.info/namespaces/middleware/tgauth">
            <SOAP-ENV:Body><SOAP-ENV:Fault>
                <faultcode>ns1:rbacFault</faultcode>
                <faultstring>2</faultstring>
                <faultactor>TgExtra</faultactor>
                <detail>
                    <ns1:rbacFaultResponse>
                        <faultNo>2</faultNo>
                        <faultMessage>This function requires an authentication and authorization.</faultMessage>
                        <cause>You are not allowed to assign the member pytest@dariah.eu to the role Beobachter,PROJECT_ID,Projekt-Teilnehmer.</cause>
                    </ns1:rbacFaultResponse>
                </detail>
            </SOAP-ENV:Fault></SOAP-ENV:Body></SOAP-ENV:Envelope>"""
        requests_mock.post(
            tgconfig.host + '/1.0/tgauth/tgextra.php',
            status_code=500,
            text=soap_mock,
        )  # we will always use the same mocked fault reply, even if the role-string would change in real requests
        sid = 'no-valid-sid'
        # we should have no right change project, especially without sid
        project_id = 'PROJECT_ID'
        eppn = 'pytest@dariah.eu'
        with pytest.raises(TextgridAuthException):
            auth.add_observer_to_project(sid, project_id, eppn)
        with pytest.raises(TextgridAuthException):
            auth.add_admin_to_project(sid, project_id, eppn)
        with pytest.raises(TextgridAuthException):
            auth.add_manager_to_project(sid, project_id, eppn)
        with pytest.raises(TextgridAuthException):
            auth.add_editor_to_project(sid, project_id, eppn)

    @staticmethod
    def test_remove_roles_without_valid_sid(auth, tgconfig, requests_mock):
        requests_mock.post(
            tgconfig.host + '/1.0/tgauth/tgextra.php',
            status_code=500,
        )  # always responding the same works
        sid = 'no-valid-sid'
        # we should have no right change project, especially without sid
        project_id = 'PROJECT_ID'
        eppn = 'pytest@dariah.eu'
        with pytest.raises(TextgridAuthException):
            auth.remove_observer_from_project(sid, project_id, eppn)
        with pytest.raises(TextgridAuthException):
            auth.remove_admin_from_project(sid, project_id, eppn)
        with pytest.raises(TextgridAuthException):
            auth.remove_manager_from_project(sid, project_id, eppn)
        with pytest.raises(TextgridAuthException):
            auth.remove_editor_from_project(sid, project_id, eppn)

    @staticmethod
    def test_add_remove_observer_role(auth, tgconfig, requests_mock):
        soap_mocks = [SOAP_RESPONSE_TRUE, SOAP_RESPONSE_TRUE]
        requests_mock.post(
            tgconfig.host + '/1.0/tgauth/tgextra.php',
            soap_mocks,
        )
        sid = 'SESSION_ID'
        project_id = 'PROJECT_ID'
        eppn = 'crud.junit1@textgrid.de'
        res = auth.add_observer_to_project(sid, project_id, eppn)
        assert res  # res shall be True
        res = auth.remove_observer_from_project(sid, project_id, eppn)
        assert res

    @staticmethod
    def test_add_remove_manager_role(auth, tgconfig, requests_mock):
        soap_mocks = [SOAP_RESPONSE_TRUE, SOAP_RESPONSE_TRUE]
        requests_mock.post(
            tgconfig.host + '/1.0/tgauth/tgextra.php',
            soap_mocks,
        )
        sid = 'SESSION_ID'
        project_id = 'PROJECT_ID'
        eppn = 'crud.junit1@textgrid.de'
        res = auth.add_manager_to_project(sid, project_id, eppn)
        assert res  # res shall be True
        res = auth.remove_manager_from_project(sid, project_id, eppn)
        assert res

    @staticmethod
    def test_add_remove_admin_role(auth, tgconfig, requests_mock):
        soap_mocks = [SOAP_RESPONSE_TRUE, SOAP_RESPONSE_TRUE]
        requests_mock.post(
            tgconfig.host + '/1.0/tgauth/tgextra.php',
            soap_mocks,
        )
        sid = 'SESSION_ID'
        project_id = 'PROJECT_ID'
        eppn = 'crud.junit1@textgrid.de'
        res = auth.add_admin_to_project(sid, project_id, eppn)
        assert res  # res shall be True
        res = auth.remove_admin_from_project(sid, project_id, eppn)
        assert res

    @staticmethod
    def test_add_remove_editor_role(auth, tgconfig, requests_mock):
        soap_mocks = [SOAP_RESPONSE_TRUE, SOAP_RESPONSE_TRUE]
        requests_mock.post(
            tgconfig.host + '/1.0/tgauth/tgextra.php',
            soap_mocks,
        )
        sid = 'SESSION_ID'
        project_id = 'PROJECT_ID'
        eppn = 'crud.junit1@textgrid.de'
        res = auth.add_editor_to_project(sid, project_id, eppn)
        assert res  # res shall be True
        res = auth.remove_editor_from_project(sid, project_id, eppn)
        assert res

    @staticmethod
    def test_eppn_for_sid_without_valid_sid(auth, tgconfig, requests_mock):
        requests_mock.post(
            tgconfig.host + '/1.0/tgauth/tgextra-crud.php',
            status_code=500,
        )
        sid = 'no-valid-sid'
        with pytest.raises(TextgridAuthException):
            auth.get_eppn_for_sid(sid)

    @staticmethod
    def test_get_ids_wrong_sid(auth, tgconfig, requests_mock):
        requests_mock.post(
            tgconfig.host + '/1.0/tgauth/tgextra.php',
            status_code=500,
        )
        sid = 'no-valid-sid'
        with pytest.raises(TextgridAuthException):
            auth.get_ids(sid, 'test', '', '')
