# SPDX-FileCopyrightText: 2022 Georg-August-Universität Göttingen
#
# SPDX-License-Identifier: LGPL-3.0-or-later

from tgclients.utils import Utils


def test_list_to_aggregation_and_back():
    textgrid_uri = 'textgrid:xyz.1'
    members = ['textgrid:1234.0', 'textrid:abcd.0', 'textgrid:4321.0']
    agg = Utils.list_to_aggregation(textgrid_uri, members)
    assert agg.startswith('<?xml version="1.0" encoding="UTF-8"?>')
    extracted_list = Utils.aggregation_to_list(agg)
    assert extracted_list == members
