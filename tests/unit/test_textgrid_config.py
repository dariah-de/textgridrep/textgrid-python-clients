# SPDX-FileCopyrightText: 2022 Georg-August-Universität Göttingen
#
# SPDX-License-Identifier: LGPL-3.0-or-later

from tgclients.config import DEFAULT_TIMEOUT, DEV_SERVER, TEST_SERVER, TextgridConfig


def test_default_config():
    """default empty constructor should be configured for textgrid production server"""
    config = TextgridConfig()
    assert config.host == 'https://textgridlab.org'
    assert config.search == 'https://textgridlab.org/1.0/tgsearch'


def test_dev_config():
    """constructor should overwrite textgrid host"""
    config = TextgridConfig('https://dev.textgridlab.org')
    assert config.host == 'https://dev.textgridlab.org'
    assert config.search == 'https://dev.textgridlab.org/1.0/tgsearch'


def test_dev_config_from_const():
    """constructor should overwrite textgrid host"""
    config = TextgridConfig(DEV_SERVER)
    assert config.host == 'https://dev.textgridlab.org'
    assert config.search == 'https://dev.textgridlab.org/1.0/tgsearch'


def test_test_config_from_const():
    """constructor should overwrite textgrid host"""
    config = TextgridConfig(TEST_SERVER)
    assert config.host == 'https://test.textgridlab.org'
    assert config.search == 'https://test.textgridlab.org/1.0/tgsearch'


def test_host_url_trailing_slash():
    """constructor should remove trailing slashes"""
    config = TextgridConfig('https://dev.textgridlab.org/')
    assert config.host == 'https://dev.textgridlab.org'
    assert config.search == 'https://dev.textgridlab.org/1.0/tgsearch'


def test_get_set_http_timeout():
    """default timeout should be overwritten by setter"""
    config = TextgridConfig()
    assert config.http_timeout == DEFAULT_TIMEOUT
    new_timeout: float = 30.2
    config.http_timeout = new_timeout
    assert config.http_timeout is new_timeout


def test_default_for_none():
    config = TextgridConfig(None)
    assert config.host == 'https://textgridlab.org'
    assert config.search == 'https://textgridlab.org/1.0/tgsearch'


def test_default_for_empty():
    config = TextgridConfig('')
    assert config.host == 'https://textgridlab.org'
    assert config.search == 'https://textgridlab.org/1.0/tgsearch'
