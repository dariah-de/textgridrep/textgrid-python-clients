# SPDX-FileCopyrightText: 2022 Georg-August-Universität Göttingen
#
# SPDX-License-Identifier: LGPL-3.0-or-later

from . import binaryFileLoader, fileLoader


class TestAggregator:
    @staticmethod
    def test_zip_single_uri(requests_mock, aggregator, tgconfig):
        """get a zip file for a single uri should be succesfull # noqa: DAR101"""
        requests_mock.get(
            tgconfig.host + '/1.0/aggregator/zip/textgrid:kv2q.0',
            content=binaryFileLoader('aggregator/zip_kv2q.0'),
        )
        uri = 'textgrid:kv2q.0'
        res = aggregator.zip(uri)
        assert res.status_code == 200

    @staticmethod
    def test_zip_urilist(requests_mock, aggregator, tgconfig):
        """get a zip file for a single uri should be succesfull # noqa: DAR101"""
        requests_mock.get(
            tgconfig.host + '/1.0/aggregator/zip/textgrid:kv2v.0,textgrid:kv2q.0',
            content=binaryFileLoader('aggregator/zip_kv2v.0_kv2q.0'),
        )
        uris = ['textgrid:kv2v.0', 'textgrid:kv2q.0']
        res = aggregator.zip(uris)
        assert res.status_code == 200

    @staticmethod
    def test_single_text(requests_mock, aggregator, tgconfig):
        """get plain text for single uri # noqa: DAR101"""
        requests_mock.get(
            tgconfig.host + '/1.0/aggregator/text/textgrid:kv2q.0',
            text=fileLoader('aggregator/text_kv2q.0'),
        )
        uri = 'textgrid:kv2q.0'
        res = aggregator.text(uri)
        assert res.status_code == 200
        assert res.text.startswith("Lewis Carroll\nAlice's Abenteuer im Wunderland")
        assert res.text.endswith('an ihr eigenes Kindesleben und die glücklichen Sommertage.\n')

    @staticmethod
    def test_more_text_uri_string(requests_mock, aggregator, tgconfig):
        """get plain text for two uris (as string) # noqa: DAR101"""
        requests_mock.get(
            tgconfig.host + '/1.0/aggregator/text/textgrid:kv2v.0,textgrid:kv2q.0',
            text=fileLoader('aggregator/text_kv2v.0_kv2q.0'),
        )
        uri = 'textgrid:kv2v.0,textgrid:kv2q.0'
        res = aggregator.text(uri)
        assert res.status_code == 200
        assert res.text.startswith('Lewis Carroll\nLewis Carroll')
        assert res.text.endswith('an ihr eigenes Kindesleben und die glücklichen Sommertage.\n')

    @staticmethod
    def test_more_text_uri_string_different_order(requests_mock, aggregator, tgconfig):
        """get plain text for two uris (as string) in reverse order # noqa: DAR101"""
        requests_mock.get(
            tgconfig.host + '/1.0/aggregator/text/textgrid:kv2q.0,textgrid:kv2v.0',
            text=fileLoader('aggregator/text_kv2q.0_kv2v.0'),
        )
        uri = 'textgrid:kv2q.0,textgrid:kv2v.0'
        res = aggregator.text(uri)
        assert res.status_code == 200
        assert res.text.startswith("Lewis Carroll\nAlice's Abenteuer im Wunderland")
        assert res.text.endswith('im Haus seiner Schwester bei Guildford und wird dort beerdigt.\n')

    @staticmethod
    def test_more_text_uri_list(requests_mock, aggregator, tgconfig):
        """get plain text for two uris (as list) # noqa: DAR101"""
        requests_mock.get(
            tgconfig.host + '/1.0/aggregator/text/textgrid:kv2v.0,textgrid:kv2q.0',
            text=fileLoader('aggregator/text_kv2v.0_kv2q.0'),
        )
        uri = ['textgrid:kv2v.0', 'textgrid:kv2q.0']
        res = aggregator.text(uri)
        assert res.status_code == 200
        assert res.text.startswith('Lewis Carroll\nLewis Carroll')
        assert res.text.endswith('an ihr eigenes Kindesleben und die glücklichen Sommertage.\n')

    @staticmethod
    def test_more_text_uri_list_different_order(requests_mock, aggregator, tgconfig):
        """get plain text for two uris (as list) in reverse order # noqa: DAR101"""
        requests_mock.get(
            tgconfig.host + '/1.0/aggregator/text/textgrid:kv2q.0,textgrid:kv2v.0',
            text=fileLoader('aggregator/text_kv2q.0_kv2v.0'),
        )
        uri = ['textgrid:kv2q.0', 'textgrid:kv2v.0']
        res = aggregator.text(uri)
        assert res.status_code == 200
        assert res.text.startswith("Lewis Carroll\nAlice's Abenteuer im Wunderland")
        assert res.text.endswith('im Haus seiner Schwester bei Guildford und wird dort beerdigt.\n')

    @staticmethod
    def test_single_teicorpus(requests_mock, aggregator, tgconfig):
        """get teicorpus for single uri # noqa: DAR101"""
        requests_mock.get(
            tgconfig.host + '/1.0/aggregator/teicorpus/textgrid:kv2q.0',
            text=fileLoader('aggregator/teicorpus_kv2q.0'),
        )
        uri = 'textgrid:kv2q.0'
        res = aggregator.teicorpus(uri)
        assert res.status_code == 200
        assert res.text.startswith("<?xml version='1.0' encoding='UTF-8'?><TEI")
        assert res.text.endswith('</TEI>')

    @staticmethod
    def test_teicorpus_uri_list(requests_mock, aggregator, tgconfig):
        """get teicorpus for two uris (as list) # noqa: DAR101"""
        requests_mock.get(
            tgconfig.host + '/1.0/aggregator/teicorpus/textgrid:kv2v.0,textgrid:kv2q.0',
            text=fileLoader('aggregator/teicorpus_kv2v.0_kv2q.0'),
        )
        uri = ['textgrid:kv2v.0', 'textgrid:kv2q.0']
        res = aggregator.teicorpus(uri)
        assert res.status_code == 200
        assert res.text.startswith("<?xml version='1.0' encoding='UTF-8'?><teiCorpus")
        assert res.text.endswith('</TEI></teiCorpus>')

    @staticmethod
    def test_single_render_stylesheet(requests_mock, aggregator, tgconfig):
        """test rendering for single uri
        with own stylesheet.
        textgrid:4228q.2 counts words # noqa: DAR101
        """
        requests_mock.get(
            tgconfig.host
            + '/1.0/aggregator/html/textgrid:kv2q.0?stylesheet=textgrid%3A4228q.2&mediatype=text%2Fplain',
            text='25598',
        )
        uri = 'textgrid:kv2q.0'
        res = aggregator.render(uri, stylesheet_uri='textgrid:4228q.2', mediatype='text/plain')
        assert res.status_code == 200
        assert res.text == '25598'

    @staticmethod
    def test_render_urilist_stylesheet(requests_mock, aggregator, tgconfig):
        """test rendering for two uris
        with own stylesheet.
        textgrid:4228q.2 counts words # noqa: DAR101
        """
        requests_mock.get(
            tgconfig.host
            + '/1.0/aggregator/html/textgrid:kv2v.0,textgrid:kv2q.0?stylesheet=textgrid%3A4228q.2&mediatype=text%2Fplain',
            text='26311',
        )
        uri = ['textgrid:kv2v.0', 'textgrid:kv2q.0']
        res = aggregator.render(uri, stylesheet_uri='textgrid:4228q.2', mediatype='text/plain')
        assert res.status_code == 200
        assert res.text == '26311'
