# SPDX-FileCopyrightText: 2022 Georg-August-Universität Göttingen
#
# SPDX-License-Identifier: LGPL-3.0-or-later

from tgclients.config import TextgridConfig
from tgclients.search import TextgridSearchRequest

from . import fileLoader


class TestTextgridSearchRequest:
    @staticmethod
    def test_empty_constructor():
        """Not passing a config in constructor should provide a client for
        tgsearch public on production system"""
        tgs = TextgridSearchRequest()
        assert tgs._url == TextgridConfig().search_public

    @staticmethod
    def test_public_info(requests_mock, searchrequest_public, tgconfig):
        requests_mock.get(
            tgconfig.host + '/1.0/tgsearch-public/info/textgrid:kv2q.0', text=fileLoader('kv2q.0')
        )

        uri = 'textgrid:kv2q.0'
        res = searchrequest_public.info(uri)
        assert res.status_code == 200
        assert res.content

    @staticmethod
    def test_public_list_project_root(requests_mock, searchrequest_public):
        # TODO: list a not to big public project root
        pass

    @staticmethod
    def test_public_aggregation_list(requests_mock, searchrequest_public, tgconfig):
        requests_mock.get(
            tgconfig.host + '/1.0/tgsearch-public/navigation/agg/textgrid:kv2p.0',
            text=fileLoader('agg_kv2p.0'),
        )
        uri = 'textgrid:kv2p.0'
        res = searchrequest_public.list_aggregation(uri)
        assert res.status_code == 200
