# SPDX-FileCopyrightText: 2022 Georg-August-Universität Göttingen
#
# SPDX-License-Identifier: LGPL-3.0-or-later

from datetime import datetime

from tgclients.metadata import TextgridMetadata

from . import binaryFileLoader


class TestTextgridCrudRequest:
    @staticmethod
    def test_create_revision_and_delete(requests_mock, crud_request, tgconfig):
        requests_mock.post(
            tgconfig.host
            + '/1.0/tgcrud/rest/create?sessionId=SESSION_ID&projectId=PROJECT_ID&createRevision=false',
            headers={'Location': 'textgrid:kv2q.0'},
            content=binaryFileLoader('crud/kv2q.0.metadata'),
        )
        requests_mock.post(
            tgconfig.host
            + '/1.0/tgcrud/rest/create?sessionId=SESSION_ID&uri=textgrid%3Akv2q.0&createRevision=true&projectId=PROJECT_ID',
            headers={'Location': 'textgrid:kv2q.1'},
            content=binaryFileLoader('crud/kv2q.1.metadata'),
        )
        requests_mock.post(
            tgconfig.host
            + '/1.0/tgcrud/rest/create?sessionId=SESSION_ID&uri=textgrid%3Akv2q.1&createRevision=true&projectId=PROJECT_ID',
            headers={'Location': 'textgrid:kv2q.2'},
            content=binaryFileLoader('crud/kv2q.2.metadata'),
        )
        requests_mock.get(
            tgconfig.host + '/1.0/tgcrud/rest/textgrid:kv2q.0/delete?sessionId=SESSION_ID',
            status_code=204,
        )
        requests_mock.get(
            tgconfig.host + '/1.0/tgcrud/rest/textgrid:kv2q.1/delete?sessionId=SESSION_ID',
            status_code=204,
        )
        requests_mock.get(
            tgconfig.host + '/1.0/tgcrud/rest/textgrid:kv2q.2/delete?sessionId=SESSION_ID',
            status_code=204,
        )
        sid = 'SESSION_ID'  # does not matter for mocked requests
        project_id = 'PROJECT_ID'  # does not matter for mocked requests
        now = datetime.now().strftime('%Y-%m-%d-%H:%M:%S')
        metadata = TextgridMetadata().create(title='test ' + now, mimetype='text/xml')
        data = '<content>here</content>'

        res_rev0 = crud_request.create_resource(sid, project_id, data, metadata)
        assert res_rev0.status_code == 200
        textgrid_uri = res_rev0.headers['Location']
        md_rev0 = res_rev0.text
        assert textgrid_uri.startswith('textgrid:')
        assert textgrid_uri.endswith('.0')

        res_rev1 = crud_request.create_revision(sid, project_id, textgrid_uri, data, md_rev0)
        assert res_rev1.status_code == 200
        textgrid_rev1_uri = res_rev1.headers['Location']
        md_rev1 = res_rev1.text
        assert textgrid_rev1_uri.startswith('textgrid:')
        assert textgrid_rev1_uri.endswith('.1')

        res_rev2 = crud_request.update_resource(
            sid, textgrid_rev1_uri, data, md_rev1, create_revision=True
        )
        assert res_rev2.status_code == 200
        textgrid_rev2_uri = res_rev2.headers['Location']
        assert textgrid_rev2_uri.startswith('textgrid:')
        assert textgrid_rev2_uri.endswith('.2')

        res = crud_request.delete_resource(sid, textgrid_rev2_uri)
        assert res.status_code == 204

        res = crud_request.delete_resource(sid, textgrid_rev1_uri)
        assert res.status_code == 204

        res = crud_request.delete_resource(sid, textgrid_uri)
        assert res.status_code == 204

    @staticmethod
    def test_error_msg_from_html(crud_request):
        html = """
               <html>
                   <head>
                       <meta name="description" content="blubber">
                   </head>
               </html>
               """

        msg = crud_request._error_msg_from_html(html)
        assert msg == 'blubber'

    @staticmethod
    def test_error_msg_from_html_no_meta(crud_request):
        html = """<html>
                      <head>
                         <title>look, error with no meta tag</title>
                     </head>
                  </html>
               """

        msg = crud_request._error_msg_from_html(html)
        assert msg.startswith('<html>')
