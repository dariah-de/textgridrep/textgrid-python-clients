# SPDX-FileCopyrightText: 2022 Georg-August-Universität Göttingen
#
# SPDX-License-Identifier: LGPL-3.0-or-later

from datetime import datetime

import pytest
from tgclients.config import TextgridConfig
from tgclients.crud import TextgridCrud, TextgridCrudException
from tgclients.metadata import TextgridMetadata

from . import binaryFileLoader


class TestTextgridCrud:
    @staticmethod
    def test_empty_constructor():
        """Not passing a config in constructor should provide a client for
        tgcrud nonpublic on production system"""
        tgc = TextgridCrud()
        assert tgc._url == TextgridConfig().crud

    @staticmethod
    def test_read_public_data(requests_mock, crud, tgconfig):
        requests_mock.get(
            tgconfig.host + '/1.0/tgcrud/rest/textgrid:kv2q.0/data',
            content=binaryFileLoader('crud/kv2q.0.data'),
        )
        uri = 'textgrid:kv2q.0'
        res = crud.read_data(uri)
        assert 'Alice fing an sich zu langweilen;' in str(res.content, 'utf-8')

    @staticmethod
    def test_read_public_metadata(requests_mock, crud, tgconfig):
        requests_mock.get(
            tgconfig.host + '/1.0/tgcrud/rest/textgrid:kv2q.0/metadata',
            content=binaryFileLoader('crud/kv2q.0.metadata'),
        )
        uri = 'textgrid:kv2q.0'
        res = crud.read_metadata(uri)
        title = res.object_value.generic.provided.title[0]
        assert title == 'Alice im Wunderland'

    @staticmethod
    def test_read_public_metadata_umlaut(requests_mock, crud, tgconfig):
        requests_mock.get(
            tgconfig.host + '/1.0/tgcrud/rest/textgrid:s60f.0/metadata',
            content=binaryFileLoader('crud/s60f.0.metadata'),
        )
        uri = 'textgrid:s60f.0'
        res = crud.read_metadata(uri)
        title = res.object_value.generic.provided.title[0]
        assert title == 'Märchen'

    @staticmethod
    def test_read_wrong_uri(requests_mock, crud, tgconfig):
        requests_mock.get(
            tgconfig.host + '/1.0/tgcrud/rest/textgrid:notexistent/metadata',
            status_code=404,
            text='<meta name="description" content="wrong uri mock message">',
        )
        uri = 'textgrid:notexistent'
        with pytest.raises(TextgridCrudException):
            crud.read_metadata(uri)

    @staticmethod
    def test_create_and_delete(requests_mock, crud, tgconfig):
        requests_mock.post(
            tgconfig.host
            + '/1.0/tgcrud/rest/create?sessionId=SESSION_ID&projectId=PROJECT_ID&createRevision=false',
            headers={'Location': 'textgrid:mocked'},
            content=binaryFileLoader('crud/kv2q.0.metadata'),
        )
        requests_mock.get(
            tgconfig.host + '/1.0/tgcrud/rest/textgrid:kv2q.0/delete?sessionId=SESSION_ID',
            status_code=204,
        )
        sid = 'SESSION_ID'  # does not matter for mocked requests
        project_id = 'PROJECT_ID'  # does not matter for mocked requests
        now = datetime.now().strftime('%Y-%m-%d-%H:%M:%S')
        metadata = TextgridMetadata().build(title='test ' + now, mimetype='text/xml')
        data = '<content>here</content>'

        res = crud.create_resource(sid, project_id, data, metadata)
        textgrid_uri = res.object_value.generic.generated.textgrid_uri.value
        assert textgrid_uri.startswith('textgrid:')
        res = crud.delete_resource(sid, textgrid_uri)
        assert res.status_code == 204

    @staticmethod
    def test_create_revision_and_delete(requests_mock, crud, tgconfig):
        requests_mock.post(
            tgconfig.host
            + '/1.0/tgcrud/rest/create?sessionId=SESSION_ID&projectId=PROJECT_ID&createRevision=false',
            headers={'Location': 'textgrid:mocked.0'},
            content=binaryFileLoader('crud/kv2q.0.metadata'),
        )
        requests_mock.post(
            tgconfig.host
            + '/1.0/tgcrud/rest/create?sessionId=SESSION_ID&uri=textgrid%3Akv2q.0&createRevision=true&projectId=PROJECT_ID',
            headers={'Location': 'textgrid:mocked.1'},
            content=binaryFileLoader('crud/kv2q.1.metadata'),
        )
        requests_mock.post(
            tgconfig.host
            + '/1.0/tgcrud/rest/create?sessionId=SESSION_ID&uri=textgrid%3Akv2q.1&createRevision=true&projectId=PROJECT_ID',
            headers={'Location': 'textgrid:mocked.2'},
            content=binaryFileLoader('crud/kv2q.2.metadata'),
        )
        requests_mock.get(
            tgconfig.host + '/1.0/tgcrud/rest/textgrid:kv2q.0/delete?sessionId=SESSION_ID',
            status_code=204,
        )
        requests_mock.get(
            tgconfig.host + '/1.0/tgcrud/rest/textgrid:kv2q.1/delete?sessionId=SESSION_ID',
            status_code=204,
        )
        requests_mock.get(
            tgconfig.host + '/1.0/tgcrud/rest/textgrid:kv2q.2/delete?sessionId=SESSION_ID',
            status_code=204,
        )
        sid = 'SESSION_ID'  # does not matter for mocked requests
        project_id = 'PROJECT_ID'  # does not matter for mocked requests
        now = datetime.now().strftime('%Y-%m-%d-%H:%M:%S')
        metadata = TextgridMetadata().build(title='test ' + now, mimetype='text/xml')
        data = '<content>here</content>'

        md_rev0 = crud.create_resource(sid, project_id, data, metadata)
        textgrid_uri = md_rev0.object_value.generic.generated.textgrid_uri.value
        assert textgrid_uri.startswith('textgrid:')
        assert textgrid_uri.endswith('.0')
        assert md_rev0.object_value.generic.generated.revision == 0

        md_rev1 = crud.create_revision(sid, project_id, textgrid_uri, data, md_rev0)
        textgrid_rev1_uri = md_rev1.object_value.generic.generated.textgrid_uri.value
        assert textgrid_rev1_uri.startswith('textgrid:')
        assert textgrid_rev1_uri.endswith('.1')
        assert md_rev1.object_value.generic.generated.revision == 1

        md_rev2 = crud.update_resource(sid, textgrid_rev1_uri, data, md_rev1, create_revision=True)
        textgrid_rev2_uri = md_rev2.object_value.generic.generated.textgrid_uri.value
        assert textgrid_rev2_uri.startswith('textgrid:')
        assert textgrid_rev2_uri.endswith('.2')
        assert md_rev2.object_value.generic.generated.revision == 2

        res = crud.delete_resource(sid, textgrid_rev2_uri)
        assert res.status_code == 204

        res = crud.delete_resource(sid, textgrid_rev1_uri)
        assert res.status_code == 204

        res = crud.delete_resource(sid, textgrid_uri)
        assert res.status_code == 204

    @staticmethod
    def test_create_update_metadata_and_delete(requests_mock, crud, tgconfig):
        requests_mock.post(
            tgconfig.host
            + '/1.0/tgcrud/rest/create?sessionId=SESSION_ID&projectId=PROJECT_ID&createRevision=false',
            headers={'Location': 'textgrid:mocked'},
            content=binaryFileLoader('crud/kv2q.0.metadata'),
        )
        requests_mock.get(
            tgconfig.host + '/1.0/tgcrud/rest/textgrid:kv2q.0/delete?sessionId=SESSION_ID',
            status_code=204,
        )
        requests_mock.get(
            tgconfig.host + '/1.0/tgcrud/rest/textgrid:kv2q.0/data?sessionId=SESSION_ID',
            content=binaryFileLoader('crud/kv2q.0.data'),
        )
        requests_mock.post(
            tgconfig.host + '/1.0/tgcrud/rest/textgrid:kv2q.0/updateMetadata?sessionId=SESSION_ID',
            content=binaryFileLoader('crud/kv2q.0.metadata'),
        )
        sid = 'SESSION_ID'  # does not matter for mocked requests
        project_id = 'PROJECT_ID'  # does not matter for mocked requests
        now = datetime.now().strftime('%Y-%m-%d-%H:%M:%S')
        title = 'Alice im Wunderland'
        metadata = TextgridMetadata().build(title=title, mimetype='text/xml')
        data = '<content>here</content>'

        cmeta = crud.create_resource(sid, project_id, data, metadata)
        textgrid_uri = cmeta.object_value.generic.generated.textgrid_uri.value
        assert textgrid_uri.startswith('textgrid:')
        assert cmeta.object_value.generic.provided.title[0] == title

        newtitle = 'new title after ' + now
        cmeta.object_value.generic.provided.title = newtitle

        # we re-use fixtures in mocked response and do not test if the metadata
        # updates (hint: it does not)
        crud.update_metadata(sid, textgrid_uri, cmeta)
        # cmeta2 = crud.update_metadata(sid, textgrid_uri, cmeta)
        # assert cmeta2.object_value.generic.provided.title[0] == newtitle

        res = crud.delete_resource(sid, textgrid_uri)
        assert res.status_code == 204

    @staticmethod
    def test_create_update_read_and_delete(requests_mock, crud, tgconfig):
        requests_mock.post(
            tgconfig.host
            + '/1.0/tgcrud/rest/create?sessionId=SESSION_ID&projectId=PROJECT_ID&createRevision=false',
            headers={'Location': 'textgrid:mocked'},
            content=binaryFileLoader('crud/kv2q.0.metadata'),
        )
        requests_mock.get(
            tgconfig.host + '/1.0/tgcrud/rest/textgrid:kv2q.0/delete?sessionId=SESSION_ID',
            status_code=204,
        )
        requests_mock.get(
            tgconfig.host + '/1.0/tgcrud/rest/textgrid:kv2q.0/data?sessionId=SESSION_ID',
            content=binaryFileLoader('crud/kv2q.0.data'),
        )
        requests_mock.get(
            tgconfig.host + '/1.0/tgcrud/rest/textgrid:kv2q.0/metadata',
            content=binaryFileLoader('crud/kv2q.0.metadata'),
        )
        requests_mock.post(
            tgconfig.host + '/1.0/tgcrud/rest/textgrid:kv2q.0/update?sessionId=SESSION_ID',
            content=binaryFileLoader('crud/kv2q.0.metadata'),
        )
        sid = 'SESSION_ID'  # does not matter for mocked requests
        project_id = 'PROJECT_ID'  # does not matter for mocked requests
        now = datetime.now().strftime('%Y-%m-%d-%H:%M:%S')
        metadata = TextgridMetadata().build(title='test ' + now, mimetype='text/plain')
        # the content on creation
        cdata = 'created'

        res = crud.create_resource(sid, project_id, cdata, metadata)
        textgrid_uri = res.object_value.generic.generated.textgrid_uri.value
        assert textgrid_uri.startswith('textgrid:')

        res = crud.read_data(textgrid_uri, sid)
        # content matches? (not with the mocked response)
        # assert cdata == str(res.content, 'utf-8')

        umetadata = crud.read_metadata(textgrid_uri, sid)
        # updated content
        udata = 'updated'
        res = crud.update_resource(sid, textgrid_uri, udata, umetadata)
        textgrid_uri = res.object_value.generic.generated.textgrid_uri.value
        assert textgrid_uri.startswith('textgrid:')

        res = crud.read_data(textgrid_uri, sid)
        # updated content matches? (not with the mocked response)
        # assert udata == str(res.content, 'utf-8')

        res = crud.delete_resource(sid, textgrid_uri)
        assert res.status_code == 204

    @staticmethod
    def test_get_uri(requests_mock, crud, tgconfig):
        requests_mock.get(
            tgconfig.host + '/1.0/tgcrud/rest/getUri?sessionId=SESSION_ID&howMany=2',
            status_code=200,
            text='textgrid:1234\ntextgrid:2345\n',
        )
        sid = 'SESSION_ID'  # does not matter for mocked requests
        uris = crud.get_uri(sid, 2)
        assert len(uris) == 2
        for uri in uris:
            assert uri.startswith('textgrid:')
