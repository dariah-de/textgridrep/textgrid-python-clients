# SPDX-FileCopyrightText: 2022 Georg-August-Universität Göttingen
#
# SPDX-License-Identifier: LGPL-3.0-or-later

from tgclients.databinding.tgsearch import Response
from tgclients.metadata import TextgridMetadata

from . import FIXTURE_PATH

filenames = {
    'jftx.0': 'Abraham_a_Sancta_Clara___Kein_Titel_.jftx.0.jpg',
    'jfsz.0': 'Biographie__Abraham_a_Sancta_Clara.jfsz.0.work',
    'jfsm.0': 'Abraham_a_Sancta_Clara.jfsm.0.collection',
    'jfsw.0': 'Satirischer_Traktat.jfsw.0.aggregation',
    'jfsx.0': 'Biographie__Abraham_a_Sancta_Clara.jfsx.0.xml',
    'noformat.1': 'Alice_im_Wunderland.noformat.1',
    'kmwm.0': 'An_Herren_J.F._Ratschky.kmwm.0.work',
    'noauth.0': 'Restricted_TextGrid_Object.noauth.0',
}


def _fixture_2_filename(filename: str) -> str:
    meta = TextgridMetadata()
    metastring = open(FIXTURE_PATH + filename, encoding='utf8').read()
    mdobj: Response = meta.searchresponse2object(metastring)
    return meta.filename_from_metadata(mdobj.result[0])


def test_filename_from_metadata():
    for key in filenames:
        print(_fixture_2_filename(key))
        assert _fixture_2_filename(key) == filenames[key]


def test_extension_from_format():
    mimetype = 'text/xml'
    ext = TextgridMetadata().extension_for_format(mimetype)
    assert ext == 'xml'

    mimetype = 'image/jpeg'
    ext = TextgridMetadata().extension_for_format(mimetype)
    assert ext == 'jpg'


def test_extension_from_unknown_format():
    mimetype = 'no/this.is.no+known+format'
    ext = TextgridMetadata().extension_for_format(mimetype)
    assert ext is None


def test_id_from_filename():
    for key in filenames:
        assert TextgridMetadata().id_from_filename(filenames[key]) == key
