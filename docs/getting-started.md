<!--
SPDX-FileCopyrightText: 2022 Georg-August-Universität Göttingen

SPDX-License-Identifier: CC0-1.0
-->

# Getting started

## Installation and Usage

Install with pip:

```sh
pip install tgclients
```

or in a Jupyter notebook:

```
!pip install tgclients
```

## Databinding for TextGrid metadata

*tgclients* provide a databinding generated with [xsdata](https://pypi.org/project/xsdata/) for dealing
with [TextGrid metadata](https://doc.textgrid.de/Metadata/). This means that for *tgsearch* and *tgcrud*
there are an implementation returning metadata with databinding (TextgridSearch, TextgridCrud) and
one low level API without databinding (TextgridSearchRequest, TextgridCrudRequest).

With the databinding you can navigate the XML metadata as Python object and do not need to use XML parsers
or XPath. E.g: `result.object_value.generic.provided.title[0]` gives you access to the first title.

To understand which metadata fields to find in what place of the TextGrid
metadata model check the [cheat sheet](https://doc.textgrid.de/attachments/metadata/cheatsheet.pdf):

[![TextGrid metadata cheat sheet](https://doc.textgrid.de/attachments/metadata/cheatsheet.png)](https://doc.textgrid.de/attachments/metadata/cheatsheet.png)


## Example Jupyter notebooks

Here are some Jupyter Notebooks with examples how to work with the tgclients.

```{toctree}
:maxdepth: 1

notebooks/Read and search public data.ipynb
notebooks/Count_all_Words.ipynb
notebooks/Retrieve text and metadata from editions.ipynb
notebooks/Kallimachos Import.ipynb
notebooks/List and read DigiBib.ipynb
```
