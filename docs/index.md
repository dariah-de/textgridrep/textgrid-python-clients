<!--
SPDX-FileCopyrightText: 2022 Georg-August-Universität Göttingen

SPDX-License-Identifier: CC0-1.0
-->

# TextGrid Python clients documentation

The TextGrid Python clients provide access to the [TextGrid Repository](https://textgridrep.org/)
services [API](http://textgridlab.org/doc/services/index.html).

You can get the latest release version from [pypi.org](https://pypi.org/project/tgclients/).

## Quick start

Find out how to [get started](getting-started.md) or view a Jupyter notebook showing [how many words the TextGrid repository](notebooks/Count_all_Words.ipynb) contains to get an idea how to work with this library.



```{toctree}
:maxdepth: 2
:caption: Usage

getting-started.md
development.md
```

```{toctree}
:maxdepth: 2
:caption: Reference

apidoc/tgclients.rst
CHANGELOG.md
```
